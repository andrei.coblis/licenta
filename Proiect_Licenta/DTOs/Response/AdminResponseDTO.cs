﻿using System;

namespace Proiect_Licenta.DTOs.Response
{
    public class AdminResponseDTO
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public string Username { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Gender { get; set; }

        public DateTime BirthDate { get; set; }
    }
}
