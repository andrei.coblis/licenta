﻿using System;

namespace Proiect_Licenta.DTOs.Response
{
    public class MessageResponseDTO
    {
        public int MessageId { get; set; }

        public string Username { get; set; }

        public string Sender { get; set; }

        public string Subject { get; set; }

        public string Description { get; set; }

        public DateTime SentDate { get; set; }
    }
}
