﻿namespace Proiect_Licenta.DTOs.Response
{
    public class MedicalDataResponseDTO
    {
        public int MedicalDataId { get; set; }

        public int Age { get; set; }

        public bool Sex { get; set; }

        public int ChestPain { get; set; }

        public int RestBp { get; set; }

        public int Cholesterol { get; set; }

        public int Fbs { get; set; }

        public int RestEcg { get; set; }

        public int Thalach { get; set; }

        public bool ExerciseAngina { get; set; }

        public float StDepression { get; set; }

        public int Slope { get; set; }

        public int NrOfBloodVessels { get; set; }

        public int Thalassemia { get; set; }

        public int Prediction { get; set; }

        public string PatientUsername { get; set; }
    }
}
