﻿using System;

namespace Proiect_Licenta.DTOs.Response
{
    public class LifestyleRecommendationResponseDTO
    {
        public int LifestyleRecommendationId { get; set; }

        public string Diet { get; set; }

        public string PhysicalActivities { get; set; }

        public DateTime LastUpdated { get; set; }

        public int PatientId { get; set; }

        public string PatientUsername { get; set; }
    }
}
