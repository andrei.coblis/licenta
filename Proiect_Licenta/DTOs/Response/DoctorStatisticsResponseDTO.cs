﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proiect_Licenta.DTOs.Response
{
    public class DoctorStatisticsResponseDTO
    {
        public int NrOfPatients { get; set; }

        public int NrOfPatientsWithMedicalData { get; set; }

        public int NrOfPatientsWithoutMedicalData { get; set; }

        public int NrOfPredictedSickPatients { get; set; }

        public int NrOfPredictedHealthyPaients { get; set; }

        public int NrOfRecommendations { get; set; }

        public int NrOfMedPlans { get; set; }
    }
}
