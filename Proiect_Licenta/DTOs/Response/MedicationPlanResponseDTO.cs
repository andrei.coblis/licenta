﻿using System;

namespace Proiect_Licenta.DTOs.Response
{
    public class MedicationPlanResponseDTO
    {
        public int PlanId { get; set; }

        public string PatientUsername { get; set; }

        public string Description { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime LastUpdated { get; set; }
    }
}
