﻿namespace Proiect_Licenta.DTOs.Response
{
    public class MedicalDataStatisticsResponseDTO
    {
        public int[] AgeHistogram { get; set; }

        public int[] RestbpHistogram { get; set; }

        public int[] CholesterolHistogram { get; set; }

        public int[] ThalachHistogram { get; set; }

        public int MaleCount { get; set; }

        public int FemaleCount { get; set; }

        public int ChestPain0Count { get; set; }

        public int ChestPain1Count { get; set; }

        public int ChestPain2Count { get; set; }

        public int ChestPain3Count { get; set; }

        public int Fbs0Count { get; set; }

        public int Fbs1Count { get; set; }

        public int RestEcg0Count { get; set; }

        public int RestEcg1Count { get; set; }

        public int RestEcg2Count { get; set; }

        public int ExerciseAnginaTrueCount { get; set; }

        public int ExerciseAnginaFalseCount { get; set; }

        public int Slope0Count { get; set; }

        public int Slope1Count { get; set; }

        public int Slope2Count { get; set; }

        public int NrOfBloodVessels0Count { get; set; }

        public int NrOfBloodVessels1Count { get; set; }

        public int NrOfBloodVessels2Count { get; set; }

        public int NrOfBloodVessels3Count { get; set; }

        public int Thalassemia0Count { get; set; }

        public int Thalassemia1Count { get; set; }

        public int Thalassemia2Count { get; set; }

        public int Thalassemia3Count { get; set; }
    }
}
