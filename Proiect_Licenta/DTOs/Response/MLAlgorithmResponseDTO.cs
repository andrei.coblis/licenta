﻿namespace Proiect_Licenta.DTOs.Response
{
    public class MLAlgorithmResponseDTO
    {
        public int MLAlgorithmId { get; set; }

        public string Name { get; set; }

        public string Parameters { get; set; }

        public float Accuracy { get; set; }

        public float Recall { get; set; }

        public float Precision { get; set; }

        public float F1Score { get; set; }

        public float ROC_AUC_Score { get; set; }
    }
}
