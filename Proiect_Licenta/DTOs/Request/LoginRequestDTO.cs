﻿using System.ComponentModel.DataAnnotations;

namespace Proiect_Licenta.DTOs.Request
{
    public class LoginRequestDTO
    {
        [Required(ErrorMessage = "Please enter an username")]
        [StringLength(20, ErrorMessage = "The {0} must be max {1} characters long.")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Please enter a password")]
        [StringLength(20, ErrorMessage = "The {0} must be max {1} characters long.")]
        public string Password { get; set; }
    }
}
