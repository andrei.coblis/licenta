﻿using System.ComponentModel.DataAnnotations;

namespace Proiect_Licenta.DTOs.Request
{
    public class PatientRequestDTO
    {
        [Required(ErrorMessage = "Please enter an user ID")]
        public int UserId { get; set; }

        [Required(ErrorMessage = "Please enter a doctor ID")]
        public int DoctorId { get; set; }
    }
}
