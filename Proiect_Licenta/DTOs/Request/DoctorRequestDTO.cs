﻿using System.ComponentModel.DataAnnotations;

namespace Proiect_Licenta.DTOs.Request
{
    public class DoctorRequestDTO
    {
        [Required(ErrorMessage = "Please enter an user ID")]
        public int UserId { get; set; }
    }
}
