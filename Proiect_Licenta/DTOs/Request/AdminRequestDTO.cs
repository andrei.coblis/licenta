﻿using System.ComponentModel.DataAnnotations;

namespace Proiect_Licenta.DTOs.Request
{
    public class AdminRequestDTO
    {
        [Required(ErrorMessage = "Please enter an user ID")]
        public int UserId { get; set; }
    }
}
