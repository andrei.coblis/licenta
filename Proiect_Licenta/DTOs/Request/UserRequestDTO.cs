﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Proiect_Licenta.DTOs.Request
{
    public class UserRequestDTO
    {
        [Required(ErrorMessage = "Please enter an username")]
        [StringLength(20, ErrorMessage = "The {0} must be max {1} characters long.")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Please enter a password")]
        [StringLength(20, ErrorMessage = "The {0} must be max {1} characters long.")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please enter a role")]
        [StringLength(15, ErrorMessage = "The {0} must be max {1} characters long.")]
        public string Role { get; set; }

        [Required(ErrorMessage = "Please enter a first name")]
        [StringLength(20, ErrorMessage = "The {0} must be max {1} characters long.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter a last name")]
        [StringLength(20, ErrorMessage = "The {0} must be max {1} characters long.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter an address")]
        [StringLength(50, ErrorMessage = "The {0} must be max {1} characters long.")]

        public string Gender { get; set; }

        [Required(ErrorMessage = "Please enter a birth date")]
        public DateTime BirthDate { get; set; }

        [StringLength(20, ErrorMessage = "The {0} must be max {1} characters long.")]
        public string DoctorUsername { get; set; }
    }
}
