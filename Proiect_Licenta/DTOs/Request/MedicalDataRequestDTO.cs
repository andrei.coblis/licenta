﻿using System.ComponentModel.DataAnnotations;

namespace Proiect_Licenta.DTOs.Request
{
    public class MedicalDataRequestDTO
    {
        [Required(ErrorMessage = "Please enter an Age")]
        public int Age { get; set; }

        [Required(ErrorMessage = "Please enter a Sex")]
        public bool Sex { get; set; }

        [Required(ErrorMessage = "Please enter a ChestPain")]
        public int ChestPain { get; set; }

        [Required(ErrorMessage = "Please enter a RestBp")]
        public float RestBp { get; set; }

        [Required(ErrorMessage = "Please enter a Cholesterol")]
        public float Cholesterol { get; set; }

        [Required(ErrorMessage = "Please enter a Fbs")]
        public float Fbs { get; set; }

        [Required(ErrorMessage = "Please enter a RestEcg")]
        public int RestEcg { get; set; }

        [Required(ErrorMessage = "Please enter a Thalach")]
        public float Thalach { get; set; }

        [Required(ErrorMessage = "Please enter an ExerciseAngina")]
        public bool ExerciseAngina { get; set; }

        [Required(ErrorMessage = "Please enter a StDepression")]
        public float StDepression { get; set; }

        [Required(ErrorMessage = "Please enter a Slope")]
        public int Slope { get; set; }

        [Required(ErrorMessage = "Please enter a NrOfBloodVessels")]
        public int NrOfBloodVessels { get; set; }

        [Required(ErrorMessage = "Please enter a Thalassemia")]
        public int Thalassemia { get; set; }

        [Required(ErrorMessage = "Please enter a PatientId")]
        public int PatientId { get; set; }
    }
}
