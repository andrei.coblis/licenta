﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Proiect_Licenta.DTOs.Request
{
    public class MessageRequestDTO
    {
        [Required(ErrorMessage = "Please enter a Username")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Please enter a Sender")]
        public string Sender { get; set; }

        [Required(ErrorMessage = "Please enter a Subject")]
        public string Subject { get; set; }

        [Required(ErrorMessage = "Please enter a Description")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Please enter a SentDate")]
        public DateTime SentDate { get; set; }
    }
}
