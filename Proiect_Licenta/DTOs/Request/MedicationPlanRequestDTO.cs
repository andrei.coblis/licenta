﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Proiect_Licenta.DTOs.Request
{
    public class MedicationPlanRequestDTO
    {
        [Required(ErrorMessage = "Please enter an Username")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Please enter a Description")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Please enter a StartDate")]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessage = "Please enter a EndDate")]
        public DateTime EndDate { get; set; }
    }
}
