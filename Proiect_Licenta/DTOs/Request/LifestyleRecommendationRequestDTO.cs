﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Proiect_Licenta.DTOs.Request
{
    public class LifestyleRecommendationRequestDTO
    {
        public string Diet { get; set; }

        public string PhysicalActivities { get; set; }

        [Required(ErrorMessage ="Please enter a PatientId")]
        public int PatientId { get; set; }
    }
}
