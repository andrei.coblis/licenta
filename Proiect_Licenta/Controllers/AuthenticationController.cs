﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.Services.Interfaces;
using Proiect_Licenta.Utility;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Proiect_Licenta.Controllers
{
        [ApiController]
        public class AuthenticationController : ControllerBase
        {
            private readonly IConfiguration _config;

            private readonly IAuthenticationService _authenticationService;

            public AuthenticationController(IConfiguration config, IAuthenticationService authenticationService)
            {
                _config = config;
                _authenticationService = authenticationService;
            }

            [AllowAnonymous]
            [HttpPost(ApiRoutes.Identity.Login)]
            public async Task<IActionResult> Login([FromBody] LoginRequestDTO loginRequestDTO)
            {
                IActionResult response = Unauthorized();
                var authenticated = await _authenticationService.CheckIfLoginCredentialsExist(loginRequestDTO);

                if (authenticated)
                {
                    var tokenString = await GenerateJSONWebToken(loginRequestDTO);
                    response = Ok(new { token = tokenString });
                }

                return response;
            }

            private async Task<string> GenerateJSONWebToken(LoginRequestDTO userInfo)
            {
                var userRole = await _authenticationService.GetUserRoleAsync(userInfo);

                var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["JwtOptions:Key"]));
                var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

                var tokenHandler = new JwtSecurityTokenHandler();
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                    new Claim("Username", userInfo.Username),
                    new Claim("Role", userRole)
                    }),
                    Expires = DateTime.Now.AddMinutes(120),
                    Issuer = _config["JwtOptions:Issuer"],
                    Audience = _config["JwtOptions:Issuer"],
                    SigningCredentials = credentials
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);
                return new JwtSecurityTokenHandler().WriteToken(token);
            }
        }
}
