﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Services.Interfaces;
using Proiect_Licenta.Utility;
using System.Threading.Tasks;

namespace Proiect_Licenta.Controllers
{
    [ApiController]
    public class MedicalDataController : ControllerBase
    {
        private readonly IMedicalDataService _medicalDataService;

        public MedicalDataController(IMedicalDataService medicalDataService)
        {
            _medicalDataService = medicalDataService;
        }

        [ProducesResponseType(typeof(MedicalDataResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.MedicalData.GetAll)]
        public async Task<IActionResult> GetAll()
        {
            var medicalData = await _medicalDataService.GetMedicalDataAsync();

            return Ok(medicalData);
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(MedicalDataResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.MedicalData.Get, Name = ApiRoutes.Names.GetMedicalData)]
        public async Task<IActionResult> Get([FromRoute] int medicalDataId)
        {
            var medicalData = await _medicalDataService.GetMedicalDataByIdAsync(medicalDataId);

            if (medicalData == null)
            {
                return NotFound();
            }

            return Ok(medicalData);
        }

        [ProducesResponseType(typeof(MedicalDataResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.MedicalData.GetByUsername)]
        public async Task<IActionResult> GetByUsername([FromRoute] string username)
        {
            var medicalData = await _medicalDataService.GetMedicalDataByUsernameAsync(username);

            return Ok(medicalData);
        }

        [ProducesResponseType(typeof(MedicalDataStatisticsResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.MedicalData.GetStatistics)]
        public async Task<IActionResult> GetStatistics()
        {
            var medicalDataStats = await _medicalDataService.GetMedicalDataStatisticsAsync();

            return Ok(medicalDataStats);
        }

        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost(ApiRoutes.MedicalData.Create)]
        public async Task<IActionResult> Create([FromBody] MedicalDataRequestDTO medicalDataRequestDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medicalData = await _medicalDataService.CreateMedicalDataAsync(medicalDataRequestDTO);

            return CreatedAtRoute(ApiRoutes.Names.GetMedicalData, new { medicalDataId = medicalData.MedicalDataId }, "New medical data created");
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete(ApiRoutes.MedicalData.Delete)]
        public async Task<IActionResult> Delete([FromRoute] int medicalDataId)
        {
            var exists = await _medicalDataService.CheckIfMedicalDataExistsAsync(medicalDataId);
            if (!exists)
            {
                return NotFound();
            }

            var deleted = await _medicalDataService.DeleteMedicalDataAsync(medicalDataId);

            if (!deleted)
            {
                return BadRequest();
            }

            return NoContent();
        }

        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPut(ApiRoutes.MedicalData.Update)]
        public async Task<IActionResult> Update([FromRoute] int medicalDataId, [FromBody] MedicalDataRequestDTO medicalDataRequestDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var exists = await _medicalDataService.CheckIfMedicalDataExistsAsync(medicalDataId);
            if (!exists)
            {
                return NotFound();
            }

            var updated = await _medicalDataService.UpdateMedicalDataAsync(medicalDataRequestDTO, medicalDataId);

            if (!updated)
            {
                return BadRequest();
            }

            return NoContent();
        }
    }
}
