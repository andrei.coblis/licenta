﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using Proiect_Licenta.Services.Interfaces;
using Proiect_Licenta.Utility;
using System.Threading.Tasks;

namespace Proiect_Licenta.Controllers
{
    [ApiController]
    public class DoctorController : ControllerBase
    {
        private readonly IDoctorService _doctorService;

        public DoctorController(IDoctorService doctorService)
        {
            _doctorService = doctorService;
        }

        [ProducesResponseType(typeof(DoctorResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.Doctors.GetAll)]
        public async Task<IActionResult> GetAll()
        {
            var doctors = await _doctorService.GetDoctorsAsync();

            return Ok(doctors);
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(DoctorResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.Doctors.Get, Name = ApiRoutes.Names.GetDoctor)]
        public async Task<IActionResult> Get([FromRoute] int doctorId)
        {
            var doctor = await _doctorService.GetDoctorByIdAsync(doctorId);

            if (doctor == null)
            {
                return NotFound();
            }

            return Ok(doctor);
        }

        [ProducesResponseType(typeof(DoctorStatisticsResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.Doctors.GetStatistics)]
        public async Task<IActionResult> GetStatistics()
        {
            var statistics = await _doctorService.GetDoctorStatisticsAsync();

            return Ok(statistics);
        }

        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost(ApiRoutes.Doctors.Create)]
        public async Task<IActionResult> Create([FromBody] DoctorRequestDTO doctorRequestDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var doctor = await _doctorService.CreateDoctorAsync(doctorRequestDTO);

            return CreatedAtRoute(ApiRoutes.Names.GetDoctor, new { doctorId = doctor.DoctorId }, "New doctor created");
        }
    }
}
