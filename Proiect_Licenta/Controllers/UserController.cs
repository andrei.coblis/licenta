﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.Entities;
using Proiect_Licenta.Services.Interfaces;
using Proiect_Licenta.Utility;
using System.Threading.Tasks;

namespace Proiect_Licenta.Controllers
{
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [ProducesResponseType(typeof(User), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.Users.GetAll)]
        public async Task<IActionResult> GetAll()
        {
            var users = await _userService.GetUsersAsync();

            return Ok(users);
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(User), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.Users.Get, Name = ApiRoutes.Names.GetUser)]
        public async Task<IActionResult> Get([FromRoute] int userId)
        {
            var user = await _userService.GetUserByIdAsync(userId);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost(ApiRoutes.Users.Create)]
        public async Task<IActionResult> Create([FromBody] UserRequestDTO userRequestDTO)
        {
            bool doctorUsernameExists = true;
            if (userRequestDTO.Role.ToLower() == "patient")
            {
                doctorUsernameExists = await _userService.CheckIfDoctorUsernameExistsAsync(userRequestDTO.DoctorUsername);
            }

            if (!doctorUsernameExists)
            {
                return BadRequest("Doctor username doesn't exist");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await _userService.CreateUserAsync(userRequestDTO);

            return CreatedAtRoute(ApiRoutes.Names.GetUser, new { userId = user.UserId }, "New user created");
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete(ApiRoutes.Users.Delete)]
        public async Task<IActionResult> Delete([FromRoute] int userId)
        {
            var exists = await _userService.CheckIfUserExistsAsync(userId);
            if (!exists)
            {
                return NotFound();
            }

            var deleted = await _userService.DeleteUserAsync(userId);

            if (!deleted)
            {
                return BadRequest();
            }

            return NoContent();
        }

        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPut(ApiRoutes.Users.Update)]
        public async Task<IActionResult> Update([FromRoute] int userId, [FromBody] UserRequestDTO userRequestDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var exists = await _userService.CheckIfUserExistsAsync(userId);
            if (!exists)
            {
                return NotFound();
            }

            var updated = await _userService.UpdateUserAsync(userRequestDTO, userId);

            if (!updated)
            {
                return BadRequest();
            }

            return NoContent();
        }
    }
}
