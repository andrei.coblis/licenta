﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Services.Interfaces;
using Proiect_Licenta.Utility;
using System.Threading.Tasks;

namespace Proiect_Licenta.Controllers
{
    [ApiController]
    public class MessageController : ControllerBase
    {
        private readonly IMessageService _messageService;

        public MessageController(IMessageService messageService)
        {
            _messageService = messageService;
        }

        [ProducesResponseType(typeof(MessageResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.Messages.GetAll)]
        public async Task<IActionResult> GetAll()
        {
            var messages = await _messageService.GetMessagesAsync();

            return Ok(messages);
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(MessageResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.Messages.Get, Name = ApiRoutes.Names.GetMessage)]
        public async Task<IActionResult> Get([FromRoute] int messageId)
        {
            var message = await _messageService.GetMessageByIdAsync(messageId);

            if (message == null)
            {
                return NotFound();
            }

            return Ok(message);
        }

        [ProducesResponseType(typeof(MessageResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.Messages.GetByUsername)]
        public async Task<IActionResult> GetByUserId([FromRoute] string username)
        {
            var messages = await _messageService.GetMessagesByUsernameAsync(username);

            return Ok(messages);
        }

        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost(ApiRoutes.Messages.Create)]
        public async Task<IActionResult> Create([FromBody] MessageRequestDTO messageRequestDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var message = await _messageService.CreateMessageAsync(messageRequestDTO);

            return CreatedAtRoute(ApiRoutes.Names.GetMessage, new { messageId = message.MessageId }, "New message created");
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete(ApiRoutes.Messages.Delete)]
        public async Task<IActionResult> Delete([FromRoute] int messageId)
        {
            var exists = await _messageService.CheckIfMessageExistsAsync(messageId);
            if (!exists)
            {
                return NotFound();
            }

            var deleted = await _messageService.DeleteMessageAsync(messageId);

            if (!deleted)
            {
                return BadRequest();
            }

            return NoContent();
        }
    }
}
