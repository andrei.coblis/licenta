﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Services.Interfaces;
using Proiect_Licenta.Utility;
using System.Threading.Tasks;

namespace Proiect_Licenta.Controllers
{
    [ApiController]
    public class MedicationPlanController : ControllerBase
    {
        private readonly IMedicationPlanService _medicationPlanService;

        public MedicationPlanController(IMedicationPlanService medicationPlanService)
        {
            _medicationPlanService = medicationPlanService;
        }

        [ProducesResponseType(typeof(MedicationPlanResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.MedicationPlans.GetAll)]
        public async Task<IActionResult> GetAll()
        {
            var medicationPlans = await _medicationPlanService.GetMedicationPlansAsync();

            return Ok(medicationPlans);
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(MedicationPlanResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.MedicationPlans.Get, Name = ApiRoutes.Names.GetMedicationPlan)]
        public async Task<IActionResult> Get([FromRoute] int medicationPlanId)
        {
            var medicationPlan = await _medicationPlanService.GetMedicationPlanByIdAsync(medicationPlanId);

            if( medicationPlan == null)
            {
                return NotFound();
            }

            return Ok(medicationPlan);
        }

        [ProducesResponseType(typeof(MedicationPlanResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.MedicationPlans.GetByUsername)]
        public async Task<IActionResult> GetByUsername([FromRoute] string username)
        {
            var medicationPlans = await _medicationPlanService.GetMedicationPlansByUsernameAsync(username);

            return Ok(medicationPlans);
        }

        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost(ApiRoutes.MedicationPlans.Create)]
        public async Task<IActionResult> Create([FromBody] MedicationPlanRequestDTO medicationPlanRequestDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medicationPlan = await _medicationPlanService.CreateMedicationPlanAsync(medicationPlanRequestDTO);

            return CreatedAtRoute(ApiRoutes.Names.GetMedicationPlan, new { medicationPlanId = medicationPlan.PlanId }, "New medication plan created");
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete(ApiRoutes.MedicationPlans.Delete)]
        public async Task<IActionResult> Delete([FromRoute] int medicationPlanId)
        {
            var exists = await _medicationPlanService.CheckIfMedicationPlanExistsAsync(medicationPlanId);
            if (!exists)
            {
                return NotFound();
            }

            var deleted = await _medicationPlanService.DeleteMedicationPlanAsync(medicationPlanId);

            if (!deleted)
            {
                return BadRequest();
            }

            return NoContent();
        }

        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPut(ApiRoutes.MedicationPlans.Update)]
        public async Task<IActionResult> Update([FromRoute] int medicationPlanId, [FromBody] MedicationPlanRequestDTO medicationPlanRequestDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var exists = await _medicationPlanService.CheckIfMedicationPlanExistsAsync(medicationPlanId);
            if (!exists)
            {
                return NotFound();
            }

            var updated = await _medicationPlanService.UpdateMedicationPlanAsync(medicationPlanRequestDTO, medicationPlanId);

            if (!updated)
            {
                return BadRequest();
            }

            return NoContent();
        }
    }
}
