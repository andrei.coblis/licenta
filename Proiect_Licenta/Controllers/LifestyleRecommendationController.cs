﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Services.Interfaces;
using Proiect_Licenta.Utility;
using System.Threading.Tasks;

namespace Proiect_Licenta.Controllers
{
    [ApiController]
    public class LifestyleRecommendationController : ControllerBase
    {
        private readonly ILifestyleRecommendationService _lifestyleRecommendationService;

        public LifestyleRecommendationController(ILifestyleRecommendationService lifestyleRecommendationService)
        {
            _lifestyleRecommendationService = lifestyleRecommendationService;
        }

        [ProducesResponseType(typeof(LifestyleRecommendationResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.LifestyleRecommendations.GetAll)]
        public async Task<IActionResult> GetAll()
        {
            var lifestyleRecommendations = await _lifestyleRecommendationService.GetLifestyleRecommendationsAsync();

            return Ok(lifestyleRecommendations);
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(LifestyleRecommendationResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.LifestyleRecommendations.Get, Name = ApiRoutes.Names.GetLifestyleRecommendation)]
        public async Task<IActionResult> Get([FromRoute] int lifestyleRecommendationId)
        {
            var lifestyleRecommendation = await _lifestyleRecommendationService.GetLifestyleRecommendationByIdAsync(lifestyleRecommendationId);

            if (lifestyleRecommendation == null)
            {
                return NotFound();
            }

            return Ok(lifestyleRecommendation);
        }

        [ProducesResponseType(typeof(LifestyleRecommendationResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.LifestyleRecommendations.GetByUsername)]
        public async Task<IActionResult> GetByUsername([FromRoute] string username)
        {
            var recommendations = await _lifestyleRecommendationService.GetRecommendationsByUsernameAsync(username);

            return Ok(recommendations);
        }

        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost(ApiRoutes.LifestyleRecommendations.Create)]
        public async Task<IActionResult> Create([FromBody] LifestyleRecommendationRequestDTO lifestyleRecommendationRequestDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var lifestyleRecommendation = await _lifestyleRecommendationService.CreateLifestyleRecommendationAsync(lifestyleRecommendationRequestDTO);

            return CreatedAtRoute(ApiRoutes.Names.GetLifestyleRecommendation, new { lifestyleRecommendationId = lifestyleRecommendation.LifestyleRecommendationId }, "New lifestyle recommendation created");
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete(ApiRoutes.LifestyleRecommendations.Delete)]
        public async Task<IActionResult> Delete([FromRoute] int recommendationId)
        {
            var exists = await _lifestyleRecommendationService.CheckIfRecommendationExistsAsync(recommendationId);
            if (!exists)
            {
                return NotFound();
            }

            var deleted = await _lifestyleRecommendationService.DeleteRecommendationAsync(recommendationId);

            if (!deleted)
            {
                return BadRequest();
            }

            return NoContent();
        }

        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPut(ApiRoutes.LifestyleRecommendations.Update)]
        public async Task<IActionResult> Update([FromRoute] int recommendationId, [FromBody] LifestyleRecommendationRequestDTO lifestyleRecommendationRequestDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var exists = await _lifestyleRecommendationService.CheckIfRecommendationExistsAsync(recommendationId);
            if (!exists)
            {
                return NotFound();
            }

            var updated = await _lifestyleRecommendationService.UpdateRecommendationAsync(lifestyleRecommendationRequestDTO, recommendationId);

            if (!updated)
            {
                return BadRequest();
            }

            return NoContent();
        }
    }
}
