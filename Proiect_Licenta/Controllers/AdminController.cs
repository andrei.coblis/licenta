﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Services.Interfaces;
using Proiect_Licenta.Utility;
using System.Threading.Tasks;

namespace Proiect_Licenta.Controllers
{
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly IAdminService _adminService;

        public AdminController(IAdminService adminService)
        {
            _adminService = adminService;
        }

        [ProducesResponseType(typeof(AdminResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.Admins.GetAll)]
        public async Task<IActionResult> GetAll()
        {
            var admins = await _adminService.GetAdminsAsync();

            return Ok(admins);
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(AdminResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.Admins.Get, Name = ApiRoutes.Names.GetAdmin)]
        public async Task<IActionResult> Get([FromRoute] int adminId)
        {
            var admin = await _adminService.GetAdminByIdAsync(adminId);

            if(admin == null)
            {
                return NotFound();
            }

            return Ok(admin);
        }

        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost(ApiRoutes.Admins.Create)]
        public async Task<IActionResult> Create([FromBody] AdminRequestDTO adminRequestDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var admin = await _adminService.CreateAdminAsync(adminRequestDTO);

            return CreatedAtRoute(ApiRoutes.Names.GetAdmin, new { adminId = admin.AdminId }, "New admin created");
        }
    }
}
