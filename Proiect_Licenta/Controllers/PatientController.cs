﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Services.Interfaces;
using Proiect_Licenta.Utility;
using System.Threading.Tasks;

namespace Proiect_Licenta.Controllers
{
    [ApiController]
    public class PatientController : ControllerBase
    {
        private readonly IPatientService _patientService;

        public PatientController(IPatientService patientService)
        {
            _patientService = patientService;
        }

        [ProducesResponseType(typeof(PatientResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.Patients.GetAll)]
        public async Task<IActionResult> GetAll()
        {
            var patients = await _patientService.GetPatientsAsync();

            return Ok(patients);
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(PatientResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.Patients.Get, Name = ApiRoutes.Names.GetPatient)]
        public async Task<IActionResult> Get([FromRoute] int patientId)
        {
            var patient = await _patientService.GetPatientByIdAsync(patientId);

            if (patient == null)
            {
                return NotFound();
            }

            return Ok(patient);
        }

        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost(ApiRoutes.Patients.Create)]
        public async Task<IActionResult> Create([FromBody] PatientRequestDTO patientRequestDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var patient = await _patientService.CreatePatientAsync(patientRequestDTO);

            return CreatedAtRoute(ApiRoutes.Names.GetPatient, new { patientId = patient.PatientId }, "New patient created");
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete(ApiRoutes.Patients.Delete)]
        public async Task<IActionResult> Delete([FromRoute] int patientId)
        {
            var exists = await _patientService.CheckIfPatientExistsAsync(patientId);
            if (!exists)
            {
                return NotFound();
            }

            var deleted = await _patientService.DeletePatientAsync(patientId);

            if(!deleted)
            {
                return BadRequest();
            }

            return NoContent();
        }
    }
}
