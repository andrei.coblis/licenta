﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Services.Interfaces;
using Proiect_Licenta.Utility;
using System.Threading.Tasks;

namespace Proiect_Licenta.Controllers
{
    [ApiController]
    public class MLAlgorithmController : ControllerBase
    {
        private readonly IMLAlgorithmService _mlAlgorithmService;

        public MLAlgorithmController(IMLAlgorithmService mLAlgorithmService)
        {
            _mlAlgorithmService = mLAlgorithmService;
        }

        [ProducesResponseType(typeof(MLAlgorithmResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.MLAlgorithm.GetAll)]
        public async Task<IActionResult> GetAll()
        {
            var mlAlgorithms = await _mlAlgorithmService.GetMLAlgorithmsAsync();

            return Ok(mlAlgorithms);
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(MLAlgorithmResponseDTO), StatusCodes.Status200OK)]
        [HttpGet(ApiRoutes.MLAlgorithm.Get, Name = ApiRoutes.Names.GetMLAlgorithm)]
        public async Task<IActionResult> Get([FromRoute] int mlAlgorithmId)
        {
            var mlAlgorithm = await _mlAlgorithmService.GetMLAlgorithmByIdAsync(mlAlgorithmId);

            if (mlAlgorithm == null)
            {
                return NotFound();
            }

            return Ok(mlAlgorithm);
        }

        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete(ApiRoutes.MLAlgorithm.Delete)]
        public async Task<IActionResult> Delete([FromRoute] int mlAlgorithmId)
        {
            var exists = await _mlAlgorithmService.CheckIfMLAlgorithmExistsAsync(mlAlgorithmId);
            if (!exists)
            {
                return NotFound();
            }

            var deleted = await _mlAlgorithmService.DeleteMLAlgorithmAsync(mlAlgorithmId);

            if (!deleted)
            {
                return BadRequest();
            }

            return NoContent();
        }
    }
}
