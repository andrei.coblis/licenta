﻿namespace Proiect_Licenta.Options
{
    public class SwaggerOptions
    {
        public string JsonRoute { get; set; }

        public string Description { get; set; }

        public string UIEndpoint { get; set; }
    }
}
