﻿using Proiect_Licenta.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Proiect_Licenta.Data
{
    public class DataInitializier
    {
        public static void Seed(DataContext context)
        {
            if (context.MedicalDatas.Any())
            {
                return;
            }

            string path = @"C:\Users\andre\Desktop\licenta\licenta_flask\heart.csv";

            using (context)
            {
                var date = new DateTime(1999, 1, 1);
                for (int i = 1; i <= 303; i++)
                {
                    date = date.AddDays(1);
                    User user = new User()
                    {
                        Username = "Patient" + i,
                        Password = "123",
                        Role = "Patient",
                        FirstName = "FirstName" + i,
                        LastName = "LastName" + i,
                        Gender = i % 2 == 0 ? "Male" : "Female",
                        BirthDate = date,
                    };

                    context.Users.Add(user);
                    context.SaveChanges();

                    Patient patient = new Patient()
                    {
                        UserId = user.UserId,
                        DoctorId = 1
                    };

                    context.Patients.Add(patient);
                    context.SaveChanges();

                }
            }

            List<MedicalData> data = File.ReadAllLines(path).Skip(1).Select(d => CreateMedicalDataFromCSV(d)).ToList();

            List<Patient> patients = context.Patients.ToList();

            using (context)
            {
                for (int i = 0; i < patients.Count; i++)
                {
                    data[i].PatientId = patients[i].PatientId;
                    context.MedicalDatas.Add(data[i]);
                    context.SaveChanges();
                }
            }
        }

        private static MedicalData CreateMedicalDataFromCSV(string csvLine)
        {
            string[] values = csvLine.Split(',');
            MedicalData medicalData = new MedicalData()
            {
                Age = int.Parse(values[0]),
                Sex = values[1].Equals("1"),
                ChestPain = int.Parse(values[2]),
                RestBp = int.Parse(values[3]),
                Cholesterol = int.Parse(values[4]),
                Fbs = int.Parse(values[5]),
                RestEcg = int.Parse(values[6]),
                Thalach = int.Parse(values[7]),
                ExerciseAngina = values[8].Equals("1"),
                StDepression = float.Parse(values[9]),
                Slope = int.Parse(values[10]),
                NrOfBloodVessels = int.Parse(values[11]),
                Thalassemia = int.Parse(values[12]),
                Prediction = int.Parse(values[13]),
                PatientId = 1,
            };

            return medicalData;
        }
    }
}
