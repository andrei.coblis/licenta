﻿using Microsoft.EntityFrameworkCore;
using Proiect_Licenta.Entities;

namespace Proiect_Licenta.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<MedicalData> MedicalDatas { get; set; }
        public DbSet<LifestyleRecommendation> LifestyleRecommendations { get; set; }
        public DbSet<MedicationPlan> MedicationPlans { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<MLAlgorithms> MLAlgorithms { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<User>()
                .HasMany(m => m.Messages)
                .WithOne(u => u.User)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<User>().HasIndex(u => u.Username).IsUnique();

            builder.Entity<Patient>()
                .HasOne(u => u.User)
                .WithOne(p => p.Patient)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Doctor>()
                .HasOne(u => u.User)
                .WithOne(d => d.Doctor)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Admin>()
                .HasOne(u => u.User)
                .WithOne(a => a.Admin)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Patient>()
                .HasMany(lr => lr.LifestyleReccomendations)
                .WithOne(p => p.Patient)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Patient>()
                .HasMany(mp => mp.MedicationPlans)
                .WithOne(p => p.Patient)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Patient>()
                .HasOne(md => md.MedicalData)
                .WithOne(p => p.Patient)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<MedicalData>()
                .HasKey(md => md.MedicalDataId);

            builder.Entity<Doctor>()
                 .HasMany(p => p.Patients)
                 .WithOne(d => d.Doctor)
                 .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
