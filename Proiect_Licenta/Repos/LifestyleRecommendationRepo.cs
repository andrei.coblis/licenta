﻿using Microsoft.EntityFrameworkCore;
using Proiect_Licenta.Data;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using Proiect_Licenta.Repos.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proiect_Licenta.Repos
{
    public class LifestyleRecommendationRepo : ILifestyleRecommendationRepo
    {
        private readonly DataContext _context;

        public LifestyleRecommendationRepo(DataContext context)
        {
            _context = context;
        }

        public async Task<bool> CheckIfRecommendationExistsAsync(int recommendationId)
        {
            using (_context)
            {
                return await _context.LifestyleRecommendations.AnyAsync(lr => lr.LifestyleRecommendationId == recommendationId);
            }
        }

        public async Task<LifestyleRecommendation> CreateLifestyleRecommendationAsync(LifestyleRecommendation lifestyleRecommendation)
        {
            using (_context)
            {
                lifestyleRecommendation.LastUpdated = DateTime.Now;
                _context.LifestyleRecommendations.Add(lifestyleRecommendation);
                await _context.SaveChangesAsync();

                return lifestyleRecommendation;
            }
        }

        public async Task<bool> DeleteRecommendationAsync(int recommendationId)
        {
            using (_context)
            {
                var recommendation = await _context.LifestyleRecommendations.FirstOrDefaultAsync(lr => lr.LifestyleRecommendationId == recommendationId);
                _context.LifestyleRecommendations.Remove(recommendation);
                var deleted = await _context.SaveChangesAsync();

                return deleted > 0;
            }
        }

        public async Task<LifestyleRecommendationResponseDTO> GetLifestyleRecommendationByIdAsync(int lifestyleRecommendationId)
        {
            using (_context)
            {
                var lifestyleRecommendations = _context.LifestyleRecommendations;
                var patients = _context.Patients;

                var lifestyleRecommendationResponseDTO = from lifestyleRecommendation in lifestyleRecommendations
                                                         join patient in patients
                                                         on lifestyleRecommendation.PatientId equals patient.PatientId
                                                         where lifestyleRecommendation.LifestyleRecommendationId == lifestyleRecommendationId
                                                         select new LifestyleRecommendationResponseDTO()
                                                         {
                                                             LifestyleRecommendationId = lifestyleRecommendation.LifestyleRecommendationId,
                                                             Diet = lifestyleRecommendation.Diet,
                                                             PhysicalActivities = lifestyleRecommendation.PhysicalActivities,
                                                             LastUpdated = lifestyleRecommendation.LastUpdated,
                                                             PatientId = lifestyleRecommendation.PatientId,
                                                             PatientUsername = patient.User.Username
                                                         };

                return await lifestyleRecommendationResponseDTO.FirstOrDefaultAsync();
            }
        }

        public async Task<List<LifestyleRecommendationResponseDTO>> GetLifestyleRecommendationsAsync()
        {
            var lifestyleRecommendations = _context.LifestyleRecommendations;
            var patients = _context.Patients;

            var lifestyleRecommendationResponseDTOList = from lifestyleRecommendation in lifestyleRecommendations
                                                         join patient in patients
                                                         on lifestyleRecommendation.PatientId equals patient.PatientId
                                                         select new LifestyleRecommendationResponseDTO()
                                                         {
                                                             LifestyleRecommendationId = lifestyleRecommendation.LifestyleRecommendationId,
                                                             Diet = lifestyleRecommendation.Diet,
                                                             PhysicalActivities = lifestyleRecommendation.PhysicalActivities,
                                                             LastUpdated = lifestyleRecommendation.LastUpdated,
                                                             PatientId = lifestyleRecommendation.PatientId,
                                                             PatientUsername = patient.User.Username
                                                         };

            return await lifestyleRecommendationResponseDTOList.ToListAsync();
        }

        public async Task<List<LifestyleRecommendationResponseDTO>> GetRecommendationsByUsernameAsync(string username)
        {
            using (_context)
            {
                var lifestyleRecommendations = _context.LifestyleRecommendations;
                var patients = _context.Patients;

                var lifestyleRecommendationResponseDTOList = from lifestyleRecommendation in lifestyleRecommendations
                                                             join patient in patients
                                                             on lifestyleRecommendation.PatientId equals patient.PatientId
                                                             where patient.User.Username == username
                                                             select new LifestyleRecommendationResponseDTO()
                                                             {
                                                                 LifestyleRecommendationId = lifestyleRecommendation.LifestyleRecommendationId,
                                                                 Diet = lifestyleRecommendation.Diet,
                                                                 PhysicalActivities = lifestyleRecommendation.PhysicalActivities,
                                                                 LastUpdated = lifestyleRecommendation.LastUpdated,
                                                                 PatientId = lifestyleRecommendation.PatientId,
                                                                 PatientUsername = patient.User.Username
                                                             };

                return await lifestyleRecommendationResponseDTOList.ToListAsync();
            }
        }

        public async Task<bool> UpdateRecommendationAsync(LifestyleRecommendation recommendation)
        {
            using (_context)
            {
                recommendation.LastUpdated = DateTime.Now;
                _context.LifestyleRecommendations.Update(recommendation);
                var updated = await _context.SaveChangesAsync();

                return updated > 0;
            }
        }
    }
}
