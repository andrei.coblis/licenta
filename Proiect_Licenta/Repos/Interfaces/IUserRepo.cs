﻿using Proiect_Licenta.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Licenta.Repos.Interfaces
{
    public interface IUserRepo
    {
        Task<List<User>> GetUsersAsync();
        Task<User> CreateUserAsync(User user, string doctorUsername);
        Task<User> GetUserByIdAsync(int userId);
        Task<bool> CheckIfUserExistsAsync(int userId);
        Task<bool> CheckIfUsernameExistsAsync(string username);
        Task<bool> CheckIfDoctorUsernameExistsAsync(string doctorUsername);
        Task<bool> DeleteUserAsync(User user);
        Task<bool> UpdateUserAsync(User user);
    }
}
