﻿using Proiect_Licenta.DTOs.Request;
using System.Threading.Tasks;

namespace Proiect_Licenta.Services.Interfaces
{
    public interface IAuthenticationRepo
    {
        Task<bool> CheckIfLoginCredentialsExist(LoginRequestDTO loginRequestDTO);
        Task<string> GetUserRole(LoginRequestDTO loginRequestDTO);
    }
}
