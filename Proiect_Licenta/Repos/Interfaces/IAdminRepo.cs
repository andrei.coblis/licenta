﻿using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proiect_Licenta.Repos.Interfaces
{
    public interface IAdminRepo
    {
        Task<List<AdminResponseDTO>> GetAdminsAsync();
        Task<AdminResponseDTO> GetAdminByIdAsync(int adminId);
        Task<Admin> CreateAdminAsync(Admin admin);
    }
}
