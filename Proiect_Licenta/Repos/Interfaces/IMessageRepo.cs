﻿using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Licenta.Repos.Interfaces
{
    public interface IMessageRepo
    {
        Task<List<MessageResponseDTO>> GetMessagesAsync();
        Task<MessageResponseDTO> GetMessageByIdAsync(int messageId);
        Task<List<MessageResponseDTO>> GetMessagesByUsernameAsync(string username);
        Task<Message> CreateMessageAsync(Message message);
        Task<bool> CheckIfMessageExistsAsync(int messageId);
        Task<bool> DeleteMessageAsync(int messageId);
    }
}
