﻿using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Licenta.Repos.Interfaces
{
    public interface IMedicalDataRepo
    {
        Task<List<MedicalDataResponseDTO>> GetMedicalDataAsync();
        Task<MedicalDataResponseDTO> GetMedicalDataByIdAsync(int medicalDataId);
        Task<MedicalDataResponseDTO> GetMedicalDataByUsernameAsync(string username);
        Task<MedicalDataStatisticsResponseDTO> GetMedicalDataStatisticsAsync();
        Task<MedicalData> CreateMedicalDataAsync(MedicalData medicalData);
        Task<bool> CheckIfMedicalDataExistsAsync(int medicalDataId);
        Task<bool> DeleteMedicalDataAsync(int medicalDataId);
        Task<bool> UpdateMedicalDataAsync(MedicalData medicalData);
    }
}
