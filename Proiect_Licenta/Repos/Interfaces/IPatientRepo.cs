﻿using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Licenta.Repos.Interfaces
{
    public interface IPatientRepo
    {
        Task<List<PatientResponseDTO>> GetPatientsAsync();
        Task<Patient> CreatePatientAsync(Patient patient);
        Task<PatientResponseDTO> GetPatientByIdAsync(int patientId);
        Task<bool> CheckIfPatientExistsAsync(int patientId);
        Task<bool> DeletePatientAsync(int patientId);
    }
}
