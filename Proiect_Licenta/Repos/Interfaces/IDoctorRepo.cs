﻿using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Licenta.Repos.Interfaces
{
    public interface IDoctorRepo
    {
        Task<List<DoctorResponseDTO>> GetDoctorsAsync();
        Task<DoctorResponseDTO> GetDoctorByIdAsync(int doctorId);
        Task<DoctorStatisticsResponseDTO> GetDoctorStatisticsAsync();
        Task<Doctor> CreateDoctorAsync(Doctor doctor); 
    }
}
