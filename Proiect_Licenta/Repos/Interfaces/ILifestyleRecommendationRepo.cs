﻿using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Licenta.Repos.Interfaces
{
    public interface ILifestyleRecommendationRepo
    {
        Task<List<LifestyleRecommendationResponseDTO>> GetLifestyleRecommendationsAsync();
        Task<List<LifestyleRecommendationResponseDTO>> GetRecommendationsByUsernameAsync(string username);
        Task<LifestyleRecommendationResponseDTO> GetLifestyleRecommendationByIdAsync(int lifestyleRecommendationId);
        Task<LifestyleRecommendation> CreateLifestyleRecommendationAsync(LifestyleRecommendation lifestyleRecommendation);
        Task<bool> CheckIfRecommendationExistsAsync(int recommendationId);
        Task<bool> DeleteRecommendationAsync(int recommendationId);
        Task<bool> UpdateRecommendationAsync(LifestyleRecommendation recommendation);
    }
}
