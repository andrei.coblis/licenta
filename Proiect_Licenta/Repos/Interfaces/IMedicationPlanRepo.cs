﻿using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Licenta.Repos.Interfaces
{
    public interface IMedicationPlanRepo
    {
        Task<bool> CheckIfMedicationPlanExistsAsync(int medicationPlanId);
        Task<List<MedicationPlanResponseDTO>> GetMedicationPlansAsync();
        Task<MedicationPlanResponseDTO> GetMedicationPlanByIdAsync(int medicationPlanId);
        Task<List<MedicationPlanResponseDTO>> GetMedicationPlanByUsernameAsync(string username);
        Task<MedicationPlan> CreateMedicationPlanAsync(MedicationPlanRequestDTO medicationPlanRequestDTO);
        Task<bool> DeleteMedicationPlanAsync(int medicationPlanId);
        //Task<bool> UpdateMedicationPlanAsync(MedicationPlan medicationPlan);
        Task<bool> UpdateMedicationPlanAsync(MedicationPlanRequestDTO medicationPlanRequestDTO, int medicationPlanId);
    }
}
