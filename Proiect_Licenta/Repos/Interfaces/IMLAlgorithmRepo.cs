﻿using Proiect_Licenta.DTOs.Response;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Licenta.Repos.Interfaces
{
    public interface IMLAlgorithmRepo
    {
        Task<List<MLAlgorithmResponseDTO>> GetMLAlgorithmsAsync();
        Task<MLAlgorithmResponseDTO> GetMLAlgorithmByIdAsync(int mlAlgorithmId);
        Task<bool> CheckIfMLAlgorithmExistsAsync(int mlAlgorithmId);
        Task<bool> DeleteMLAlgorithmAsync(int mlAlgorithmId);
    }
}
