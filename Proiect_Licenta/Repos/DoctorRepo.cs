﻿using Proiect_Licenta.Data;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using Proiect_Licenta.Repos.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Proiect_Licenta.Repos
{
    public class DoctorRepo : IDoctorRepo
    {
        private readonly DataContext _context;

        public DoctorRepo(DataContext dataContext)
        {
            _context = dataContext;
        }

        public async Task<Doctor> CreateDoctorAsync(Doctor doctor)
        {
            using (_context)
            {
                _context.Doctors.Add(doctor);
                await _context.SaveChangesAsync();

                return doctor;
            }
        }

        public async Task<DoctorResponseDTO> GetDoctorByIdAsync(int doctorId)
        {
            using (_context)
            {
                var doctors = _context.Doctors;
                var users = _context.Users;

                var doctorResponseDTO = from doctor in doctors
                                        join user in users
                                        on doctor.UserId equals user.UserId
                                        where doctor.DoctorId == doctorId
                                        select new DoctorResponseDTO()
                                        {
                                            Id = doctor.DoctorId,
                                            UserId = doctor.UserId,
                                            Username = user.Username,
                                            FirstName = user.FirstName,
                                            LastName = user.LastName,
                                            Gender = user.Gender,
                                            BirthDate = user.BirthDate
                                        };

                return await doctorResponseDTO.FirstOrDefaultAsync();
            }
        }

        public async Task<List<DoctorResponseDTO>> GetDoctorsAsync()
        {
            using (_context)
            {
                var doctors = _context.Doctors;
                var users = _context.Users;

                var doctorResponseDTOList = from doctor in doctors
                                            join user in users
                                            on doctor.UserId equals user.UserId
                                            select new DoctorResponseDTO()
                                            {
                                                Id = doctor.DoctorId,
                                                UserId = doctor.UserId,
                                                Username = user.Username,
                                                FirstName = user.FirstName,
                                                LastName = user.LastName,
                                                Gender = user.Gender,
                                                BirthDate = user.BirthDate
                                            };

                return await doctorResponseDTOList.ToListAsync();
            }
        }

        public async Task<DoctorStatisticsResponseDTO> GetDoctorStatisticsAsync()
        {
            using (_context)
            {
                var patients = _context.Patients;
                var medicationPlans = _context.MedicationPlans;
                var recommendations = _context.LifestyleRecommendations;

                var doctorStatisticsResponseDTO = new DoctorStatisticsResponseDTO()
                {
                    NrOfPatients = patients.Count(),
                    NrOfMedPlans = medicationPlans.Count(),
                    NrOfRecommendations = recommendations.Count(),
                    NrOfPatientsWithMedicalData = patients.Count(p => p.MedicalData != null),
                    NrOfPatientsWithoutMedicalData = patients.Count(p => p.MedicalData == null),
                    NrOfPredictedHealthyPaients = patients.Count(p => p.MedicalData.Prediction == 0),
                    NrOfPredictedSickPatients = patients.Count(p => p.MedicalData.Prediction == 1)
                };

                return doctorStatisticsResponseDTO;
            }
        }
    }
}
