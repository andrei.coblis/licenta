﻿using Microsoft.EntityFrameworkCore;
using Proiect_Licenta.Data;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using Proiect_Licenta.Repos.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Proiect_Licenta.Repos
{
    public class PatientRepo : IPatientRepo
    {
        private readonly DataContext _context;

        public PatientRepo(DataContext context)
        {
            _context = context;
        }

        public async Task<bool> CheckIfPatientExistsAsync(int patientId)
        {
            using (_context)
            {
                return await _context.Patients.AnyAsync(p => p.PatientId == patientId);
            }
        }

        public async Task<Patient> CreatePatientAsync(Patient patient)
        {
            using (_context)
            {
                _context.Add(patient);
                await _context.SaveChangesAsync();

                return patient;
            }
        }

        public async Task<bool> DeletePatientAsync(int patientId)
        {
            using(_context)
            {
                var patient = await _context.Patients.FirstOrDefaultAsync(p => p.PatientId == patientId);
                _context.Patients.Remove(patient);
                var deleted = await _context.SaveChangesAsync();

                return deleted > 0;
            }
        }

        public async Task<PatientResponseDTO> GetPatientByIdAsync(int patientId)
        {
            using (_context)
            {
                var patients = _context.Patients;
                var users = _context.Users;

                var medicalData = _context.MedicalDatas;
                var hasMedicalData = await medicalData.AnyAsync(md => md.PatientId == patientId);

                var patientResponseDTO = from patient in patients
                                         join user in users
                                         on patient.UserId equals user.UserId
                                         join md in medicalData
                                         on patient.MedicalData.PatientId equals md.PatientId
                                         where patient.PatientId == patientId
                                         select new PatientResponseDTO()
                                         {
                                             Id = patient.PatientId,
                                             UserId = user.UserId,
                                             Username = user.Username,
                                             DoctorUsername = patient.Doctor.User.Username,
                                             FirstName = user.FirstName,
                                             LastName = user.LastName,
                                             Gender = user.Gender,
                                             BirthDate = user.BirthDate,
                                             HasMedicalData = hasMedicalData,
                                             MedicalDataId = md.MedicalDataId
                                         };

                return await patientResponseDTO.FirstOrDefaultAsync();
            }
        }

        public async Task<List<PatientResponseDTO>> GetPatientsAsync()
        {
            using (_context)
            {
                var patients = _context.Patients;
                var users = _context.Users;
                var medicalData = _context.MedicalDatas;

                var patientResponseDTOList = from patient in patients
                                             join user in users
                                             on patient.UserId equals user.UserId
                                             select new PatientResponseDTO()
                                             {
                                                 Id = patient.PatientId,
                                                 UserId = user.UserId,
                                                 Username = user.Username,
                                                 DoctorUsername = patient.Doctor.User.Username,
                                                 FirstName = user.FirstName,
                                                 LastName = user.LastName,
                                                 Gender = user.Gender,
                                                 BirthDate = user.BirthDate,
                                                 HasMedicalData = patient.MedicalData != null,
                                                 MedicalDataId = patient.MedicalData.MedicalDataId
                                             };

                return await patientResponseDTOList.ToListAsync();
            }
        }
    }
}
