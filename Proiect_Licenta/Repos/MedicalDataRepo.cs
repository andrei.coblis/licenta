﻿using Microsoft.EntityFrameworkCore;
using Proiect_Licenta.Data;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using Proiect_Licenta.Repos.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proiect_Licenta.Repos
{
    public class MedicalDataRepo : IMedicalDataRepo
    {
        private readonly DataContext _context;

        public MedicalDataRepo(DataContext context)
        {
            _context = context;
        }

        public async Task<bool> CheckIfMedicalDataExistsAsync(int medicalDataId)
        {
            using (_context)
            {
                return await _context.MedicalDatas.AnyAsync(md => md.MedicalDataId == medicalDataId);
            }
        }

        public async Task<MedicalData> CreateMedicalDataAsync(MedicalData medicalData)
        {
            using (_context)
            {
                _context.MedicalDatas.Add(medicalData);
                await _context.SaveChangesAsync();

                return medicalData;
            }
        }

        public async Task<bool> DeleteMedicalDataAsync(int medicalDataId)
        {
            using (_context)
            {
                var medicalData = await _context.MedicalDatas.FirstOrDefaultAsync(md => md.MedicalDataId == medicalDataId);
                _context.MedicalDatas.Remove(medicalData);

                var deleted = await _context.SaveChangesAsync();

                return deleted > 0;
            }
        }

        public async Task<List<MedicalDataResponseDTO>> GetMedicalDataAsync()
        {
            using (_context)
            {
                var patients = _context.Patients;
                var medicalDatas = _context.MedicalDatas;

                var medicalDataResponseDTOList = from medicalData in medicalDatas
                                             join patient in patients
                                             on medicalData.PatientId equals patient.PatientId
                                             select new MedicalDataResponseDTO()
                                             {
                                                 MedicalDataId = medicalData.MedicalDataId,
                                                 Age = medicalData.Age,
                                                 Sex = medicalData.Sex,
                                                 ChestPain = medicalData.ChestPain,
                                                 RestBp = medicalData.RestBp,
                                                 Cholesterol = medicalData.Cholesterol,
                                                 Fbs = medicalData.Fbs,
                                                 RestEcg = medicalData.RestEcg,
                                                 Thalach = medicalData.Thalach,
                                                 ExerciseAngina = medicalData.ExerciseAngina,
                                                 StDepression = medicalData.StDepression,
                                                 Slope = medicalData.Slope,
                                                 NrOfBloodVessels = medicalData.NrOfBloodVessels,
                                                 Thalassemia = medicalData.Thalassemia,
                                                 Prediction = medicalData.Prediction,
                                                 PatientUsername = patient.User.Username
                                             };

                return await medicalDataResponseDTOList.ToListAsync();
            }
        }

        public async Task<MedicalDataResponseDTO> GetMedicalDataByIdAsync(int medicalDataId)
        {
            using (_context)
            {
                var patients = _context.Patients;
                var medicalDatas = _context.MedicalDatas;

                var medicalDataResponseDTO = from medicalData in medicalDatas
                                             join patient in patients
                                             on medicalData.PatientId equals patient.PatientId
                                             where medicalData.MedicalDataId == medicalDataId
                                             select new MedicalDataResponseDTO()
                                             {
                                                 MedicalDataId = medicalData.MedicalDataId,
                                                 Age = medicalData.Age,
                                                 Sex = medicalData.Sex,
                                                 ChestPain = medicalData.ChestPain,
                                                 RestBp = medicalData.RestBp,
                                                 Cholesterol = medicalData.Cholesterol,
                                                 Fbs = medicalData.Fbs,
                                                 RestEcg = medicalData.RestEcg,
                                                 Thalach = medicalData.Thalach,
                                                 ExerciseAngina = medicalData.ExerciseAngina,
                                                 StDepression = medicalData.StDepression,
                                                 Slope = medicalData.Slope,
                                                 NrOfBloodVessels = medicalData.NrOfBloodVessels,
                                                 Thalassemia = medicalData.Thalassemia,
                                                 Prediction = medicalData.Prediction,
                                                 PatientUsername = patient.User.Username
                                             };

                return await medicalDataResponseDTO.FirstOrDefaultAsync();
            }
        }

        public async Task<MedicalDataResponseDTO> GetMedicalDataByUsernameAsync(string username)
        {
            using (_context)
            {
                var patients = _context.Patients;
                var medicalDatas = _context.MedicalDatas;

                var medicalDataResponseDTO = from medicalData in medicalDatas
                                             join patient in patients
                                             on medicalData.PatientId equals patient.PatientId
                                             where patient.User.Username == username
                                             select new MedicalDataResponseDTO()
                                             {
                                                 MedicalDataId = medicalData.MedicalDataId,
                                                 Age = medicalData.Age,
                                                 Sex = medicalData.Sex,
                                                 ChestPain = medicalData.ChestPain,
                                                 RestBp = medicalData.RestBp,
                                                 Cholesterol = medicalData.Cholesterol,
                                                 Fbs = medicalData.Fbs,
                                                 RestEcg = medicalData.RestEcg,
                                                 Thalach = medicalData.Thalach,
                                                 ExerciseAngina = medicalData.ExerciseAngina,
                                                 StDepression = medicalData.StDepression,
                                                 Slope = medicalData.Slope,
                                                 NrOfBloodVessels = medicalData.NrOfBloodVessels,
                                                 Thalassemia = medicalData.Thalassemia,
                                                 Prediction = medicalData.Prediction,
                                                 PatientUsername = patient.User.Username
                                             };

                return await medicalDataResponseDTO.FirstOrDefaultAsync();
            }
        }

        public async Task<MedicalDataStatisticsResponseDTO> GetMedicalDataStatisticsAsync()
        {
            using (_context)
            {
                int[] ageHistogram = new int[100];
                int[] restbpHistogram = new int[220];
                int[] cholesterolHistogram = new int[600];
                int[] thalachHistogram = new int[220];
                var medicalDatas = _context.MedicalDatas;
                int maleCount, femaleCount, chestPain0Count, chestPain1Count, chestPain2Count, chestPain3Count, fbs0Count, fbs1Count, restEcg0Count, restEcg1Count, restEcg2Count, 
                    exerciseAnginaTrueCount, exerciseAnginaFalseCount, slope0Count, slope1Count, slope2Count, nrOfBloodVessels0Count, nrOfBloodVessels1Count, nrOfBloodVessels2Count,
                    nrOfBloodVessels3Count, thalassemia0Count, thalassemia1Count, thalassemia2Count, thalassemia3Count;
                maleCount = femaleCount = chestPain0Count  = chestPain1Count  = chestPain2Count  = chestPain3Count = fbs0Count = fbs1Count = restEcg0Count = restEcg1Count = restEcg2Count =
                exerciseAnginaTrueCount = exerciseAnginaFalseCount = slope0Count = slope1Count = slope2Count = nrOfBloodVessels0Count = nrOfBloodVessels1Count = nrOfBloodVessels2Count =
                nrOfBloodVessels3Count = thalassemia0Count = thalassemia1Count = thalassemia2Count = thalassemia3Count = 0;

                foreach (var medicalData in medicalDatas.ToList())
                {
                    ageHistogram[medicalData.Age]++;
                    restbpHistogram[medicalData.RestBp]++;
                    cholesterolHistogram[medicalData.Cholesterol]++;
                    thalachHistogram[medicalData.Thalach]++;

                    if (medicalData.Sex)
                    {
                        maleCount++;
                    }
                    else
                    {
                        femaleCount++;
                    }

                    switch (medicalData.ChestPain)
                    {
                        case (0):
                            chestPain0Count++;
                            break;
                        case (1):
                            chestPain1Count++;
                            break;
                        case (2):
                            chestPain2Count++;
                            break;
                        case (3):
                            chestPain3Count++;
                            break;
                        default:
                            break;
                    }

                    if (medicalData.Fbs == 0)
                    {
                        fbs0Count++;
                    }
                    else
                    {
                        fbs1Count++;
                    }

                    switch (medicalData.RestEcg)
                    {
                        case (0):
                            restEcg0Count++;
                            break;
                        case (1):
                            restEcg1Count++;
                            break;
                        case (2):
                            restEcg2Count++;
                            break;
                        default:
                            break;
                    }

                    if (medicalData.ExerciseAngina)
                    {
                        exerciseAnginaTrueCount++;
                    }
                    else
                    {
                        exerciseAnginaFalseCount++;
                    }

                    switch (medicalData.Slope)
                    {
                        case (0):
                            slope0Count++;
                            break;
                        case (1):
                            slope1Count++;
                            break;
                        case (2):
                            slope2Count++;
                            break;
                        default:
                            break;
                    }

                    switch (medicalData.NrOfBloodVessels)
                    {
                        case (0):
                            nrOfBloodVessels0Count++;
                            break;
                        case (1):
                            nrOfBloodVessels1Count++;
                            break;
                        case (2):
                            nrOfBloodVessels2Count++;
                            break;
                        case (3):
                            nrOfBloodVessels3Count++;
                            break;
                        default:
                            break;
                    }

                    switch (medicalData.Thalassemia)
                    {
                        case (0):
                            thalassemia0Count++;
                            break;
                        case (1):
                            thalassemia1Count++;
                            break;
                        case (2):
                            thalassemia2Count++;
                            break;
                        case (3):
                            thalassemia3Count++;
                            break;
                        default:
                            break;
                    }
                }

                var medicalDataStatsResponseDTO = new MedicalDataStatisticsResponseDTO()
                {
                    AgeHistogram = ageHistogram,
                    RestbpHistogram = restbpHistogram,
                    CholesterolHistogram = cholesterolHistogram,
                    ThalachHistogram = thalachHistogram,
                    MaleCount = maleCount,
                    FemaleCount = femaleCount,
                    ChestPain0Count = chestPain0Count,
                    ChestPain1Count = chestPain1Count,
                    ChestPain2Count = chestPain2Count,
                    ChestPain3Count = chestPain3Count,
                    Fbs0Count = fbs0Count,
                    Fbs1Count = fbs1Count,
                    RestEcg0Count = restEcg0Count,
                    RestEcg1Count = restEcg1Count,
                    RestEcg2Count = restEcg2Count,
                    ExerciseAnginaTrueCount = exerciseAnginaTrueCount,
                    ExerciseAnginaFalseCount = exerciseAnginaFalseCount,
                    Slope0Count = slope0Count,
                    Slope1Count = slope1Count,
                    Slope2Count = slope2Count,
                    NrOfBloodVessels0Count = nrOfBloodVessels0Count,
                    NrOfBloodVessels1Count = nrOfBloodVessels1Count,
                    NrOfBloodVessels2Count = nrOfBloodVessels2Count,
                    NrOfBloodVessels3Count = nrOfBloodVessels3Count,
                    Thalassemia0Count = thalassemia0Count,
                    Thalassemia1Count = thalassemia1Count,
                    Thalassemia2Count = thalassemia2Count,
                    Thalassemia3Count = thalassemia3Count
                };

                return medicalDataStatsResponseDTO;
            }
        }

        public async Task<bool> UpdateMedicalDataAsync(MedicalData medicalData)
        {
            using (_context)
            {
                _context.MedicalDatas.Update(medicalData);
                var updated = await _context.SaveChangesAsync();

                return updated > 0;
            }
        }
    }
}
