﻿using Microsoft.EntityFrameworkCore;
using Proiect_Licenta.Data;
using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using Proiect_Licenta.Repos.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proiect_Licenta.Repos
{
    public class MedicationPlanRepo : IMedicationPlanRepo
    {
        private readonly DataContext _context;

        public MedicationPlanRepo(DataContext context)
        {
            _context = context;
        }

        public async Task<bool> CheckIfMedicationPlanExistsAsync(int medicationPlanId)
        {
            using (_context)
            {
                return await _context.MedicationPlans.AnyAsync(mp => mp.PlanId == medicationPlanId);
            }
        }

        public async Task<MedicationPlan> CreateMedicationPlanAsync(MedicationPlanRequestDTO medicationPlanRequestDTO)
        {
            using (_context)
            {
                var patient = await _context.Patients.FirstOrDefaultAsync(p => p.User.Username == medicationPlanRequestDTO.Username);

                var medicationPlan = new MedicationPlan()
                {
                    PatientId = patient.PatientId,
                    Description = medicationPlanRequestDTO.Description,
                    StartDate = medicationPlanRequestDTO.StartDate,
                    EndDate = medicationPlanRequestDTO.EndDate,
                    LastUpdated = System.DateTime.Now
                };

                _context.MedicationPlans.Add(medicationPlan);
                await _context.SaveChangesAsync();

                return medicationPlan;
            }
        }

        public async Task<bool> DeleteMedicationPlanAsync(int medicationPlanId)
        {
            using (_context)
            {
                var medicationPlan = await _context.MedicationPlans.FirstOrDefaultAsync(mp => mp.PlanId == medicationPlanId);
                _context.MedicationPlans.Remove(medicationPlan);

                var deleted = await _context.SaveChangesAsync();

                return deleted > 0;
            }
        }

        public async Task<MedicationPlanResponseDTO> GetMedicationPlanByIdAsync(int medicationPlanId)
        {
            using (_context)
            {
                var medicationPlans = _context.MedicationPlans;
                var patients = _context.Patients;

                var medicationPlanResponseDTO = from medicationPlan in medicationPlans
                                                join patient in patients
                                                on medicationPlan.PatientId equals patient.PatientId
                                                where medicationPlan.PlanId == medicationPlanId
                                                select new MedicationPlanResponseDTO()
                                                {
                                                    PlanId = medicationPlan.PlanId,
                                                    PatientUsername = patient.User.Username,
                                                    Description = medicationPlan.Description,
                                                    StartDate = medicationPlan.StartDate,
                                                    EndDate = medicationPlan.EndDate,
                                                    LastUpdated = medicationPlan.LastUpdated
                                                };

                return await medicationPlanResponseDTO.FirstOrDefaultAsync();
            }
        }

        public async Task<List<MedicationPlanResponseDTO>> GetMedicationPlanByUsernameAsync(string username)
        {
            using (_context)
            {
                var medicationPlans = _context.MedicationPlans;
                var patients = _context.Patients;

                var medicationPlanResponseDTOList = from medicationPlan in medicationPlans
                                                    join patient in patients
                                                    on medicationPlan.PatientId equals patient.PatientId
                                                    where patient.User.Username == username
                                                    select new MedicationPlanResponseDTO()
                                                    {
                                                        PlanId = medicationPlan.PlanId,
                                                        PatientUsername = patient.User.Username,
                                                        Description = medicationPlan.Description,
                                                        StartDate = medicationPlan.StartDate,
                                                        EndDate = medicationPlan.EndDate,
                                                        LastUpdated = medicationPlan.LastUpdated
                                                    };

                return await medicationPlanResponseDTOList.ToListAsync();
            }
        }

        public async Task<List<MedicationPlanResponseDTO>> GetMedicationPlansAsync()
        {
            using (_context)
            {
                var medicationPlans = _context.MedicationPlans;
                var patients = _context.Patients;

                var medicationPlanResponseDTOList = from medicationPlan in medicationPlans
                                                    join patient in patients
                                                    on medicationPlan.PatientId equals patient.PatientId
                                                    select new MedicationPlanResponseDTO()
                                                    {
                                                        PlanId = medicationPlan.PlanId,
                                                        PatientUsername = patient.User.Username,
                                                        Description = medicationPlan.Description,
                                                        StartDate = medicationPlan.StartDate,
                                                        EndDate = medicationPlan.EndDate,
                                                        LastUpdated = medicationPlan.LastUpdated
                                                    };

                return await medicationPlanResponseDTOList.ToListAsync();
            }
        }

        public async Task<bool> UpdateMedicationPlanAsync(MedicationPlanRequestDTO medicationPlanRequestDTO, int medicationPlanId)
        {
            using (_context)
            {
                var patient = await _context.Patients.FirstOrDefaultAsync(p => p.User.Username == medicationPlanRequestDTO.Username);

                var medicationPlan = new MedicationPlan()
                {
                    PlanId = medicationPlanId,
                    PatientId = patient.PatientId,
                    Description = medicationPlanRequestDTO.Description,
                    StartDate = medicationPlanRequestDTO.StartDate,
                    EndDate = medicationPlanRequestDTO.EndDate,
                    LastUpdated = System.DateTime.Now
                };

                _context.MedicationPlans.Update(medicationPlan);
                var updated = await _context.SaveChangesAsync();

                return updated > 0;
            }
        }
    }
}
