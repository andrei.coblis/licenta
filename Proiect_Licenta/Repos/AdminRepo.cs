﻿using Microsoft.EntityFrameworkCore;
using Proiect_Licenta.Data;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using Proiect_Licenta.Repos.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proiect_Licenta.Repos
{
    public class AdminRepo : IAdminRepo
    {
        private readonly DataContext _context;

        public AdminRepo(DataContext dataContext)
        {
            _context = dataContext;
        }

        public async Task<Admin> CreateAdminAsync(Admin admin)
        {
            using (_context)
            {
                _context.Admins.Add(admin);
                await _context.SaveChangesAsync();

                return admin;
            }
        }

        public async Task<AdminResponseDTO> GetAdminByIdAsync(int adminId)
        {
            using (_context)
            {
                var admins = _context.Admins;
                var users = _context.Users;

                var adminResponseDTO = from admin in admins
                                       join user in users
                                       on admin.UserId equals user.UserId
                                       where admin.AdminId == adminId
                                       select new AdminResponseDTO()
                                       {
                                           Id = admin.AdminId,
                                           UserId = admin.UserId,
                                           Username = user.Username,
                                           FirstName = user.FirstName,
                                           LastName = user.LastName,
                                           Gender = user.Gender,
                                           BirthDate = user.BirthDate
                                       };

                return await adminResponseDTO.FirstOrDefaultAsync();
            }
        }

        public async Task<List<AdminResponseDTO>> GetAdminsAsync()
        {
            using (_context)
            {
                var admins = _context.Admins;
                var users = _context.Users;

                var adminResponseDTOList = from admin in admins
                                           join user in users
                                           on admin.UserId equals user.UserId
                                           select new AdminResponseDTO()
                                           {
                                               Id = admin.AdminId,
                                               UserId = admin.UserId,
                                               Username = user.Username,
                                               FirstName = user.FirstName,
                                               LastName = user.LastName,
                                               Gender = user.Gender,
                                               BirthDate = user.BirthDate
                                           };

                return await adminResponseDTOList.ToListAsync();
            }
        }
    }
}
