﻿using Microsoft.EntityFrameworkCore;
using Proiect_Licenta.Data;
using Proiect_Licenta.Entities;
using Proiect_Licenta.Repos.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Licenta.Repos
{
    public class UserRepo : IUserRepo
    {
        private readonly DataContext _context;

        public UserRepo(DataContext context)
        {
            _context = context;
        }

        public async Task<bool> CheckIfDoctorUsernameExistsAsync(string doctorUsername)
        {
            using (_context)
            {
                return await _context.Users.AnyAsync(u => u.Username == doctorUsername);
            }
        }

        public async Task<bool> CheckIfUserExistsAsync(int userId)
        {
            using (_context)
            {
                return await _context.Users.AnyAsync(u => u.UserId == userId);
            }
        }

        public async Task<bool> CheckIfUsernameExistsAsync(string username)
        {
            using (_context)
            {
                return await _context.Users.AnyAsync(u => u.Username == username);
            }
        }

        public async Task<User> CreateUserAsync(User user, string doctorUsername)
        {
            using (_context)
            {
                var patients = _context.Patients;
                var doctors = _context.Doctors;
                var admins = _context.Admins;

                _context.Users.Add(user);
                await _context.SaveChangesAsync();

                switch (user.Role.ToLower())
                {
                    case ("patient"):
                        var doctorTemp = await doctors.FirstOrDefaultAsync(d => d.User.Username == doctorUsername);
                        var patient = new Patient() { UserId = user.UserId, DoctorId = doctorTemp.DoctorId};
                        patients.Add(patient);
                        await _context.SaveChangesAsync();
                        break;
                    case ("doctor"):
                        var doctor = new Doctor() { UserId = user.UserId };
                        doctors.Add(doctor);
                        await _context.SaveChangesAsync();
                        break;
                    case ("admin"):
                        var admin = new Admin() { UserId = user.UserId };
                        admins.Add(admin);
                        await _context.SaveChangesAsync();
                        break;
                    default:
                        break;
                }

                return user;
            }
        }

        public async Task<bool> DeleteUserAsync(User user)
        {
            using (_context)
            {
                _context.Users.Remove(user);
                var deleted = await _context.SaveChangesAsync();

                return deleted > 0;
            }
        }

        public async Task<User> GetUserByIdAsync(int userId)
        {
            using (_context)
            {
                return await _context.Users.FirstOrDefaultAsync(u => u.UserId == userId);
            }
        }

        public async Task<List<User>> GetUsersAsync()
        {
            using (_context)
            {
                return await _context.Users.ToListAsync();
            }
        }

        public async Task<bool> UpdateUserAsync(User user)
        {
            using (_context)
            {
                _context.Users.Update(user);
                var updated = await _context.SaveChangesAsync();

                return updated > 0;
            }
        }
    }
}
