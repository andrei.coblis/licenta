﻿using Microsoft.EntityFrameworkCore;
using Proiect_Licenta.Data;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using Proiect_Licenta.Repos.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proiect_Licenta.Repos
{
    public class MessageRepo : IMessageRepo
    {
        private readonly DataContext _context;

        public MessageRepo(DataContext context)
        {
            _context = context;
        }

        public async Task<bool> CheckIfMessageExistsAsync(int messageId)
        {
            using (_context)
            {
                return await _context.Messages.AnyAsync(m => m.MessageId == messageId);
            }
        }

        public async Task<Message> CreateMessageAsync(Message message)
        {
            using (_context)
            {
                _context.Messages.Add(message);
                await _context.SaveChangesAsync();

                return message;
            }
        }

        public async Task<bool> DeleteMessageAsync(int messageId)
        {
            using (_context)
            {
                var message = await _context.Messages.FirstOrDefaultAsync(m => m.MessageId == messageId);
                _context.Messages.Remove(message);
                var deleted = await _context.SaveChangesAsync();

                return deleted > 0;
            }
        }

        public async Task<MessageResponseDTO> GetMessageByIdAsync(int messageId)
        {
            using (_context)
            {
                var messages = _context.Messages;
                var users = _context.Users;

                var messageResponseDTO = from message in messages
                                         join user in users
                                         on message.Username equals user.Username
                                         where message.MessageId == messageId
                                         select new MessageResponseDTO()
                                         {
                                             MessageId = message.MessageId,
                                             Username = user.Username,
                                             Sender = message.Sender,
                                             Subject = message.Subject,
                                             Description = message.Description,
                                             SentDate = message.SentDate
                                         };

                return await messageResponseDTO.FirstOrDefaultAsync();
            }
        }

        public async Task<List<MessageResponseDTO>> GetMessagesAsync()
        {
            using (_context)
            {
                var messages = _context.Messages;
                var users = _context.Users;

                var messageResponseDTOList = from message in messages
                                             join user in users
                                             on message.Username equals user.Username
                                             select new MessageResponseDTO()
                                             {
                                                 MessageId = message.MessageId,
                                                 Username = user.Username,
                                                 Sender = message.Sender,
                                                 Subject = message.Subject,
                                                 Description = message.Description,
                                                 SentDate = message.SentDate
                                             };

                return await messageResponseDTOList.ToListAsync();
            }
        }

        public async Task<List<MessageResponseDTO>> GetMessagesByUsernameAsync(string username)
        {
            using (_context)
            {
                var messages = _context.Messages;
                var users = _context.Users;

                var messageResponseDTOList = from message in messages
                                             join user in users
                                             on message.Username equals user.Username
                                             where user.Username == username
                                             select new MessageResponseDTO()
                                             {
                                                 MessageId = message.MessageId,
                                                 Username = user.Username,
                                                 Sender = message.Sender,
                                                 Subject = message.Subject,
                                                 Description = message.Description,
                                                 SentDate = message.SentDate
                                             };

                return await messageResponseDTOList.ToListAsync();
            }
        }
    }
}
