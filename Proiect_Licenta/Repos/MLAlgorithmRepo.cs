﻿using Microsoft.EntityFrameworkCore;
using Proiect_Licenta.Data;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Repos.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proiect_Licenta.Repos
{
    public class MLAlgorithmRepo : IMLAlgorithmRepo
    {
        private readonly DataContext _context;

        public MLAlgorithmRepo(DataContext context)
        {
            _context = context;
        }

        public async Task<bool> CheckIfMLAlgorithmExistsAsync(int mlAlgorithmId)
        {
            using (_context)
            {
                return await _context.MLAlgorithms.AnyAsync(ml => ml.MLAlgorithmId == mlAlgorithmId);
            }
        }

        public async Task<bool> DeleteMLAlgorithmAsync(int mlAlgorithmId)
        {
            using (_context)
            {
                var mlAlgorithm = await _context.MLAlgorithms.FirstOrDefaultAsync(ml => ml.MLAlgorithmId == mlAlgorithmId);
                _context.MLAlgorithms.Remove(mlAlgorithm);

                var deleted = await _context.SaveChangesAsync();

                return deleted > 0;
            }
        }

        public async Task<MLAlgorithmResponseDTO> GetMLAlgorithmByIdAsync(int mlAlgorithmId)
        {
            using (_context)
            {
                var mlAlgorithms = _context.MLAlgorithms;

                var mlAlgorithmResponseDTO = from mlAlgorithm in mlAlgorithms
                                             where mlAlgorithm.MLAlgorithmId == mlAlgorithmId
                                             select new MLAlgorithmResponseDTO()
                                             {
                                                 MLAlgorithmId = mlAlgorithm.MLAlgorithmId,
                                                 Name = mlAlgorithm.Name,
                                                 Parameters = mlAlgorithm.Parameters,
                                                 Accuracy = mlAlgorithm.Accuracy,
                                                 Precision = mlAlgorithm.Precision,
                                                 Recall = mlAlgorithm.Recall,
                                                 F1Score = mlAlgorithm.F1Score,
                                                 ROC_AUC_Score = mlAlgorithm.ROC_AUC_Score
                                             };

                return await mlAlgorithmResponseDTO.FirstOrDefaultAsync();
            }
        }

        public async Task<List<MLAlgorithmResponseDTO>> GetMLAlgorithmsAsync()
        {
            using (_context)
            {
                var mlAlgorithms = _context.MLAlgorithms;

                var mlAlgorithmResponseDTOList = from mlAlgorithm in mlAlgorithms
                                                 select new MLAlgorithmResponseDTO()
                                                 {
                                                     MLAlgorithmId = mlAlgorithm.MLAlgorithmId,
                                                     Name = mlAlgorithm.Name,
                                                     Parameters = mlAlgorithm.Parameters,
                                                     Accuracy = mlAlgorithm.Accuracy,
                                                     Precision = mlAlgorithm.Precision,
                                                     Recall = mlAlgorithm.Recall,
                                                     F1Score = mlAlgorithm.F1Score,
                                                     ROC_AUC_Score = mlAlgorithm.ROC_AUC_Score
                                                 };

                return await mlAlgorithmResponseDTOList.ToListAsync();
            }
        }
    }
}
