﻿using Microsoft.EntityFrameworkCore;
using Proiect_Licenta.Data;
using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.Services.Interfaces;
using System.Threading.Tasks;

namespace Proiect_Licenta.Repos
{
    public class AuthenticationRepo : IAuthenticationRepo
    {
        private readonly DataContext _context;

        public AuthenticationRepo(DataContext context)
        {
            _context = context;
        }

        public async Task<bool> CheckIfLoginCredentialsExist(LoginRequestDTO loginRequestDTO)
        {
            using (_context)
            {
                return await _context.Users.AnyAsync(u => u.Username == loginRequestDTO.Username && u.Password == loginRequestDTO.Password);
            }
        }

        public async Task<string> GetUserRole(LoginRequestDTO loginRequestDTO)
        {
            using (_context)
            {
                var user = await _context.Users.FirstOrDefaultAsync(u => u.Username == loginRequestDTO.Username && u.Password == loginRequestDTO.Password);
                return user.Role;
            }
        }
    }
}
