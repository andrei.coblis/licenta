using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Proiect_Licenta.Data;
using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.Options;
using Proiect_Licenta.Repos;
using Proiect_Licenta.Repos.Interfaces;
using Proiect_Licenta.Services.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;
using Proiect_Licenta.Services;
using Microsoft.AspNetCore.Mvc;

namespace Proiect_Licenta
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var jwtOptions = new JwtOptions();
            Configuration.GetSection(nameof(JwtOptions)).Bind(jwtOptions);

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = jwtOptions.Issuer,
                    ValidAudience = jwtOptions.Issuer,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtOptions.Key))
                };
            });

            services.AddControllers().AddNewtonsoftJson(options => {}).SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services.AddDbContextPool<DataContext>(
                options =>
                {
                    options.UseMySQL(Configuration.GetConnectionString("DBConnection"));
                }
            );

            services.AddSwaggerGen(x =>
            {
                x.SwaggerDoc("v1", new OpenApiInfo { Title = "Licenta Api", Version = "v1" });

                x.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                x.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,
                        },
                        new List<string>()
                    }
                 });
            });

            services.AddSwaggerGen();

            services.AddScoped<IUserRepo, UserRepo>();

            services.AddScoped<IUserService, UserService>();

            services.AddScoped<IAuthenticationRepo, AuthenticationRepo>();

            services.AddScoped<IAuthenticationService, AuthenticationService>();

            services.AddScoped<IPatientRepo, PatientRepo>();

            services.AddScoped<IPatientService, PatientService>();

            services.AddScoped<IDoctorRepo, DoctorRepo>();

            services.AddScoped<IDoctorService, DoctorService>();

            services.AddScoped<IAdminRepo, AdminRepo>();

            services.AddScoped<IAdminService, AdminService>();

            services.AddScoped<IMedicationPlanRepo, MedicationPlanRepo>();

            services.AddScoped<IMedicationPlanService, MedicationPlanService>();

            services.AddScoped<ILifestyleRecommendationRepo, LifestyleRecommendationRepo>();

            services.AddScoped<ILifestyleRecommendationService, LifestyleRecommendationService>();

            services.AddScoped<IMessageRepo, MessageRepo>();

            services.AddScoped<IMessageService, MessageService>();

            services.AddScoped<IMedicalDataRepo, MedicalDataRepo>();

            services.AddScoped<IMedicalDataService, MedicalDataService>();

            services.AddScoped<IMLAlgorithmRepo, MLAlgorithmRepo>();

            services.AddScoped<IMLAlgorithmService, MLAlgorithmService>();

            services.AddAutoMapper(typeof(Startup));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var swaggerOptions = new SwaggerOptions();
            Configuration.GetSection(nameof(SwaggerOptions)).Bind(swaggerOptions);

            app.UseSwagger(option =>
            {
                option.RouteTemplate = swaggerOptions.JsonRoute;
            });

            app.UseSwaggerUI(option =>
            {
                option.SwaggerEndpoint(swaggerOptions.UIEndpoint, swaggerOptions.Description);
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(opt =>
            {
                opt.AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod();
            });

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}
