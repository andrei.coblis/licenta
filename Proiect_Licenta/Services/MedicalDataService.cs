﻿using AutoMapper;
using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using Proiect_Licenta.Repos.Interfaces;
using Proiect_Licenta.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Licenta.Services
{
    public class MedicalDataService : IMedicalDataService
    {

        private readonly IMedicalDataRepo _medicalDataRepo;
        private readonly IMapper _mapper;

        public MedicalDataService(IMedicalDataRepo medicalDataRepo, IMapper mapper)
        {
            _medicalDataRepo = medicalDataRepo;
            _mapper = mapper;
        }

        public async Task<bool> CheckIfMedicalDataExistsAsync(int medicalDataId)
        {
            return await _medicalDataRepo.CheckIfMedicalDataExistsAsync(medicalDataId);
        }

        public async Task<MedicalData> CreateMedicalDataAsync(MedicalDataRequestDTO medicalDataRequestDTO)
        {
            var medicalData = _mapper.Map<MedicalData>(medicalDataRequestDTO);
            return await _medicalDataRepo.CreateMedicalDataAsync(medicalData);
        }

        public async Task<bool> DeleteMedicalDataAsync(int medicalDataId)
        {
            return await _medicalDataRepo.DeleteMedicalDataAsync(medicalDataId);
        }

        public async Task<List<MedicalDataResponseDTO>> GetMedicalDataAsync()
        {
            return await _medicalDataRepo.GetMedicalDataAsync();
        }

        public async Task<MedicalDataResponseDTO> GetMedicalDataByIdAsync(int medicalDataId)
        {
            return await _medicalDataRepo.GetMedicalDataByIdAsync(medicalDataId);
        }

        public async Task<MedicalDataResponseDTO> GetMedicalDataByUsernameAsync(string username)
        {
            return await _medicalDataRepo.GetMedicalDataByUsernameAsync(username);
        }

        public async Task<MedicalDataStatisticsResponseDTO> GetMedicalDataStatisticsAsync()
        {
            return await _medicalDataRepo.GetMedicalDataStatisticsAsync();
        }

        public async Task<bool> UpdateMedicalDataAsync(MedicalDataRequestDTO medicalDataRequestDTO, int medicalDataId)
        {
            var medicalData = _mapper.Map<MedicalData>(medicalDataRequestDTO);
            medicalData.MedicalDataId = medicalDataId;

            return await _medicalDataRepo.UpdateMedicalDataAsync(medicalData);
        }
    }
}
