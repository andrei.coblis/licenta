﻿using AutoMapper;
using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using Proiect_Licenta.Repos.Interfaces;
using Proiect_Licenta.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proiect_Licenta.Services
{
    public class PatientService : IPatientService
    {
        private readonly IPatientRepo _patientRepository;
        private readonly IMapper _mapper;

        public PatientService(IPatientRepo patientRepository, IMapper mapper)
        {
            _patientRepository = patientRepository;
            _mapper = mapper;
        }

        public async Task<bool> CheckIfPatientExistsAsync(int patientId)
        {
            return await _patientRepository.CheckIfPatientExistsAsync(patientId);
        }

        public async Task<Patient> CreatePatientAsync(PatientRequestDTO patientRequestDTO)
        {
            var patient = _mapper.Map<Patient>(patientRequestDTO);
            return await _patientRepository.CreatePatientAsync(patient);
        }

        public async Task<bool> DeletePatientAsync(int patientId)
        {
            return await _patientRepository.DeletePatientAsync(patientId);
        }

        public async Task<PatientResponseDTO> GetPatientByIdAsync(int patientId)
        {
            return await _patientRepository.GetPatientByIdAsync(patientId);
        }

        public async Task<List<PatientResponseDTO>> GetPatientsAsync()
        {
            return await _patientRepository.GetPatientsAsync();
        }

        //public async Task<bool> UpdatePatientAsync(PatientRequestDTO patientRequestDTO, int patientId)
        //{
        //    var patient = _mapper.Map<Patient>(patientRequestDTO);
        //    patient.PatientId = patientId;

        //    return await _patientRepository.UpdatePatient(patient);
        //}
    }
}
