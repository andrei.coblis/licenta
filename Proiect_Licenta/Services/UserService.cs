﻿using AutoMapper;
using Proiect_Licenta.Entities;
using Proiect_Licenta.Repos.Interfaces;
using Proiect_Licenta.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Licenta.DTOs.Request
{
    public class UserService : IUserService
    {
        private readonly IUserRepo _userRepo;
        private readonly IMapper _mapper;

        public UserService(IUserRepo userRepo, IMapper mapper)
        {
            _userRepo = userRepo;
            _mapper = mapper;
        }

        public async Task<bool> CheckIfDoctorUsernameExistsAsync(string doctorUsername)
        {
            return await _userRepo.CheckIfDoctorUsernameExistsAsync(doctorUsername);
        }

        public async Task<bool> CheckIfUserExistsAsync(int userId)
        {
            return await _userRepo.CheckIfUserExistsAsync(userId);
        }

        public async Task<bool> CheckIfUsernameExistsAsync(string username)
        {
            return await _userRepo.CheckIfUsernameExistsAsync(username);
        }

        public async Task<User> CreateUserAsync(UserRequestDTO userRequestDTO)
        {
            var user = _mapper.Map<User>(userRequestDTO);
            var doctorUsername = userRequestDTO.DoctorUsername;
            return await _userRepo.CreateUserAsync(user, doctorUsername);
        }

        public async Task<bool> DeleteUserAsync(int userId)
        {
            var user = await GetUserByIdAsync(userId);

            return await _userRepo.DeleteUserAsync(user);
        }

        public async Task<User> GetUserByIdAsync(int userId)
        {
            return await _userRepo.GetUserByIdAsync(userId);
        }

        public async Task<List<User>> GetUsersAsync()
        {
            return await _userRepo.GetUsersAsync();
        }

        public async Task<bool> UpdateUserAsync(UserRequestDTO userRequestDTO, int userId)
        {
            var user = _mapper.Map<User>(userRequestDTO);
            user.UserId = userId;

            return await _userRepo.UpdateUserAsync(user);
        }
    }
}
