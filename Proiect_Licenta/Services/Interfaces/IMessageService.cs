﻿using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Licenta.Services.Interfaces
{
    public interface IMessageService
    {
        Task<List<MessageResponseDTO>> GetMessagesAsync();
        Task<MessageResponseDTO> GetMessageByIdAsync(int messageId);
        Task<List<MessageResponseDTO>> GetMessagesByUsernameAsync(string username);
        Task<Message> CreateMessageAsync(MessageRequestDTO messageRequestDTO);
        Task<bool> CheckIfMessageExistsAsync(int messageId);
        Task<bool> DeleteMessageAsync(int messageId);
    }
}
