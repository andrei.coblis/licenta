﻿using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Licenta.Services.Interfaces
{
    public interface IMedicationPlanService
    {
        Task<bool> CheckIfMedicationPlanExistsAsync(int medicationPlanId);
        Task<List<MedicationPlanResponseDTO>> GetMedicationPlansAsync();
        Task<MedicationPlanResponseDTO> GetMedicationPlanByIdAsync(int medicationPlanId);
        Task<List<MedicationPlanResponseDTO>> GetMedicationPlansByUsernameAsync(string username);
        Task<MedicationPlan> CreateMedicationPlanAsync(MedicationPlanRequestDTO medicationPlanRequestDTO);
        Task<bool> DeleteMedicationPlanAsync(int medicationPlanId);
        Task<bool> UpdateMedicationPlanAsync(MedicationPlanRequestDTO medicationPlanRequestDTO, int medicationPlanId);
    }
}
