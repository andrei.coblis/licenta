﻿using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Licenta.Services.Interfaces
{
    public interface IMedicalDataService
    {
        Task<List<MedicalDataResponseDTO>> GetMedicalDataAsync();
        Task<MedicalDataResponseDTO> GetMedicalDataByIdAsync(int medicalDataId);
        Task<MedicalDataResponseDTO> GetMedicalDataByUsernameAsync(string username);
        Task<MedicalDataStatisticsResponseDTO> GetMedicalDataStatisticsAsync();
        Task<bool> CheckIfMedicalDataExistsAsync(int medicalDataId);
        Task<MedicalData> CreateMedicalDataAsync(MedicalDataRequestDTO medicalDataRequestDTO);
        Task<bool> DeleteMedicalDataAsync(int medicalDataId);
        Task<bool> UpdateMedicalDataAsync(MedicalDataRequestDTO medicalDataRequestDTO, int medicalDataId);
    }
}
