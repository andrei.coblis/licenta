﻿using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proiect_Licenta.Services.Interfaces
{
    public interface ILifestyleRecommendationService
    {
        Task<List<LifestyleRecommendationResponseDTO>> GetLifestyleRecommendationsAsync();
        Task<List<LifestyleRecommendationResponseDTO>> GetRecommendationsByUsernameAsync(string username);
        Task<LifestyleRecommendationResponseDTO> GetLifestyleRecommendationByIdAsync(int lifestyleRecommendationId);
        Task<LifestyleRecommendation> CreateLifestyleRecommendationAsync(LifestyleRecommendationRequestDTO lifestyleRecommendationRequestDTO);
        Task<bool> CheckIfRecommendationExistsAsync(int recommendationId);
        Task<bool> DeleteRecommendationAsync(int recommendationId);
        Task<bool> UpdateRecommendationAsync(LifestyleRecommendationRequestDTO lifestyleRecommendationRequestDTO, int recommendationId);
    }
}
