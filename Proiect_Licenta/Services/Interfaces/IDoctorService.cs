﻿using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Licenta.Services.Interfaces
{
    public interface IDoctorService
    {
        Task<List<DoctorResponseDTO>> GetDoctorsAsync();
        Task<DoctorResponseDTO> GetDoctorByIdAsync(int doctorId);
        Task<DoctorStatisticsResponseDTO> GetDoctorStatisticsAsync();
        Task<Doctor> CreateDoctorAsync(DoctorRequestDTO doctorRequestDTO);
    }
}
