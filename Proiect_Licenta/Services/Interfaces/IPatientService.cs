﻿using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Licenta.Services.Interfaces
{
    public interface IPatientService
    {
        Task<List<PatientResponseDTO>> GetPatientsAsync();
        Task<PatientResponseDTO> GetPatientByIdAsync(int patientId);
        Task<Patient> CreatePatientAsync(PatientRequestDTO patientRequestDTO);
        Task<bool> CheckIfPatientExistsAsync(int patientId);
        Task<bool> DeletePatientAsync(int patientId);
    }
}
