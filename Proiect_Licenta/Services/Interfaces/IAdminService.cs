﻿using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Licenta.Services.Interfaces
{
    public interface IAdminService
    {
        Task<List<AdminResponseDTO>> GetAdminsAsync();
        Task<AdminResponseDTO> GetAdminByIdAsync(int adminId);
        Task<Admin> CreateAdminAsync(AdminRequestDTO adminRequestDTO);
    }
}
