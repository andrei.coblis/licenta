﻿using Proiect_Licenta.DTOs.Request;
using System.Threading.Tasks;

namespace Proiect_Licenta.Services.Interfaces
{
    public interface IAuthenticationService
    {
        Task<bool> CheckIfLoginCredentialsExist(LoginRequestDTO loginRequestDTO);
        Task<string> GetUserRoleAsync(LoginRequestDTO loginRequestDTO);
    }
}
