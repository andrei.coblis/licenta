﻿using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Licenta.Services.Interfaces
{
    public interface IUserService
    {
        Task<List<User>> GetUsersAsync();
        Task<User> GetUserByIdAsync(int userId);
        Task<User> CreateUserAsync(UserRequestDTO userRequestDTO);
        Task<bool> CheckIfUserExistsAsync(int userId);
        Task<bool> CheckIfUsernameExistsAsync(string username);
        Task<bool> CheckIfDoctorUsernameExistsAsync(string doctorUsername);
        Task<bool> DeleteUserAsync(int userId);
        Task<bool> UpdateUserAsync(UserRequestDTO userRequestDTO, int userId);
    }
}
