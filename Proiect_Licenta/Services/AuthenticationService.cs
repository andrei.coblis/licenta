﻿using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.Services.Interfaces;
using System.Threading.Tasks;

namespace Proiect_Licenta.Services
{
    public class AuthenticationService :IAuthenticationService
    {
        private readonly IAuthenticationRepo _authenticationRepository;

        public AuthenticationService(IAuthenticationRepo authenticationRepository)
        {
            _authenticationRepository = authenticationRepository;
        }

        public async Task<bool> CheckIfLoginCredentialsExist(LoginRequestDTO loginRequestDTO)
        {
            return await _authenticationRepository.CheckIfLoginCredentialsExist(loginRequestDTO);
        }

        public async Task<string> GetUserRoleAsync(LoginRequestDTO loginRequestDTO)
        {
            return await _authenticationRepository.GetUserRole(loginRequestDTO);
        }
    }
}
