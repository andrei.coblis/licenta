﻿using AutoMapper;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Repos.Interfaces;
using Proiect_Licenta.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proiect_Licenta.Services
{
    public class MLAlgorithmService : IMLAlgorithmService
    {
        private readonly IMLAlgorithmRepo _mlAlgorithmRepo;
        private readonly IMapper _mapper;

        public MLAlgorithmService(IMLAlgorithmRepo mLAlgorithmRepo, IMapper mapper)
        {
            _mlAlgorithmRepo = mLAlgorithmRepo;
        }
        public async Task<bool> CheckIfMLAlgorithmExistsAsync(int mlAlgorithmId)
        {
            return await _mlAlgorithmRepo.CheckIfMLAlgorithmExistsAsync(mlAlgorithmId);
        }

        public async Task<bool> DeleteMLAlgorithmAsync(int mlAlgorithmId)
        {
            return await _mlAlgorithmRepo.DeleteMLAlgorithmAsync(mlAlgorithmId);
        }

        public async Task<MLAlgorithmResponseDTO> GetMLAlgorithmByIdAsync(int mlAlgorithmId)
        {
            return await _mlAlgorithmRepo.GetMLAlgorithmByIdAsync(mlAlgorithmId);
        }

        public async Task<List<MLAlgorithmResponseDTO>> GetMLAlgorithmsAsync()
        {
            return await _mlAlgorithmRepo.GetMLAlgorithmsAsync();
        }
    }
}
