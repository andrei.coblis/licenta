﻿using AutoMapper;
using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using Proiect_Licenta.Repos.Interfaces;
using Proiect_Licenta.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Licenta.Services
{
    public class MedicationPlanService : IMedicationPlanService
    {
        private readonly IMedicationPlanRepo _medicationPlanRepo;
        private readonly IMapper _mapper;

        public MedicationPlanService(IMedicationPlanRepo medicationPlanRepo, IMapper mapper)
        {
            _medicationPlanRepo = medicationPlanRepo;
            _mapper = mapper;
        }

        public async Task<MedicationPlan> CreateMedicationPlanAsync(MedicationPlanRequestDTO medicationPlanRequestDTO)
        {
            return await _medicationPlanRepo.CreateMedicationPlanAsync(medicationPlanRequestDTO);
        }

        public async Task<MedicationPlanResponseDTO> GetMedicationPlanByIdAsync(int medicationPlanId)
        {
            return await _medicationPlanRepo.GetMedicationPlanByIdAsync(medicationPlanId);
        }

        public async Task<List<MedicationPlanResponseDTO>> GetMedicationPlansByUsernameAsync(string username)
        {
            return await _medicationPlanRepo.GetMedicationPlanByUsernameAsync(username);
        }

        public async Task<List<MedicationPlanResponseDTO>> GetMedicationPlansAsync()
        {
            return await _medicationPlanRepo.GetMedicationPlansAsync();
        }

        public async Task<bool> CheckIfMedicationPlanExistsAsync(int medicationPlanId)
        {
            return await _medicationPlanRepo.CheckIfMedicationPlanExistsAsync(medicationPlanId);
        }

        public async Task<bool> DeleteMedicationPlanAsync(int medicationPlanId)
        {
            return await _medicationPlanRepo.DeleteMedicationPlanAsync(medicationPlanId);
        }

        public async Task<bool> UpdateMedicationPlanAsync(MedicationPlanRequestDTO medicationPlanRequestDTO, int medicationPlanId)
        {
            //var medicationPlan = _mapper.Map<MedicationPlan>(medicationPlanRequestDTO);
            //medicationPlan.PlanId = medicationPlanId;
            return await _medicationPlanRepo.UpdateMedicationPlanAsync(medicationPlanRequestDTO, medicationPlanId);
        }
    }
}
