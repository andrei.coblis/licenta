﻿using AutoMapper;
using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using Proiect_Licenta.Repos.Interfaces;
using Proiect_Licenta.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Licenta.Services
{
    public class AdminService : IAdminService
    {
        private readonly IAdminRepo _adminRepo;
        private readonly IMapper _mapper;

        public AdminService(IAdminRepo adminRepo, IMapper mapper)
        {
            _adminRepo = adminRepo;
            _mapper = mapper;
        }

        public async Task<Admin> CreateAdminAsync(AdminRequestDTO adminRequestDTO)
        {
            var admin = _mapper.Map<Admin>(adminRequestDTO);
            return await _adminRepo.CreateAdminAsync(admin);
        }

        public async Task<AdminResponseDTO> GetAdminByIdAsync(int adminId)
        {
            return await _adminRepo.GetAdminByIdAsync(adminId);
        }

        public async Task<List<AdminResponseDTO>> GetAdminsAsync()
        {
            return await _adminRepo.GetAdminsAsync();
        }
    }
}
