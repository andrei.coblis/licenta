﻿using AutoMapper;
using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using Proiect_Licenta.Repos.Interfaces;
using Proiect_Licenta.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Licenta.Services
{
    public class MessageService : IMessageService
    {
        private readonly IMessageRepo _messageRepo;
        private readonly IMapper _mapper;

        public MessageService(IMessageRepo messageRepo, IMapper mapper)
        {
            _messageRepo = messageRepo;
            _mapper = mapper;
        }

        public async Task<bool> CheckIfMessageExistsAsync(int messageId)
        {
            return await _messageRepo.CheckIfMessageExistsAsync(messageId);
        }

        public async Task<Message> CreateMessageAsync(MessageRequestDTO messageRequestDTO)
        {
            var message = _mapper.Map<Message>(messageRequestDTO);
            return await _messageRepo.CreateMessageAsync(message);
        }

        public async Task<bool> DeleteMessageAsync(int messageId)
        {
            return await _messageRepo.DeleteMessageAsync(messageId);
        }

        public async Task<MessageResponseDTO> GetMessageByIdAsync(int messageId)
        {
            return await _messageRepo.GetMessageByIdAsync(messageId);
        }

        public async Task<List<MessageResponseDTO>> GetMessagesAsync()
        {
            return await _messageRepo.GetMessagesAsync();
        }

        public async Task<List<MessageResponseDTO>> GetMessagesByUsernameAsync(string username)
        {
            return await _messageRepo.GetMessagesByUsernameAsync(username);
        }
    }
}
