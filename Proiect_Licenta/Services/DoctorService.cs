﻿using AutoMapper;
using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using Proiect_Licenta.Repos.Interfaces;
using Proiect_Licenta.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Licenta.Services
{
    public class DoctorService : IDoctorService
    {
        private readonly IDoctorRepo _doctorRepo;
        private readonly IMapper _mapper;

        public DoctorService(IDoctorRepo doctorRepo, IMapper mapper)
        {
            _doctorRepo = doctorRepo;
            _mapper = mapper;
        }

        public async Task<Doctor> CreateDoctorAsync(DoctorRequestDTO doctorRequestDTO)
        {
            var doctor = _mapper.Map<Doctor>(doctorRequestDTO);
            return await _doctorRepo.CreateDoctorAsync(doctor);
        }

        public async Task<DoctorResponseDTO> GetDoctorByIdAsync(int doctorId)
        {
            return await _doctorRepo.GetDoctorByIdAsync(doctorId);
        }

        public async Task<List<DoctorResponseDTO>> GetDoctorsAsync()
        {
            return await _doctorRepo.GetDoctorsAsync();
        }

        public async Task<DoctorStatisticsResponseDTO> GetDoctorStatisticsAsync()
        {
            return await _doctorRepo.GetDoctorStatisticsAsync();
        }
    }
}
