﻿using AutoMapper;
using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.DTOs.Response;
using Proiect_Licenta.Entities;
using Proiect_Licenta.Repos.Interfaces;
using Proiect_Licenta.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Licenta.Services
{
    public class LifestyleRecommendationService : ILifestyleRecommendationService
    {
        private readonly ILifestyleRecommendationRepo _lifestyleRecommendationRepo;
        private readonly IMapper _mapper;

        public LifestyleRecommendationService(ILifestyleRecommendationRepo lifestyleRecommendationRepo, IMapper mapper)
        {
            _lifestyleRecommendationRepo = lifestyleRecommendationRepo;
            _mapper = mapper;
        }

        public async Task<bool> CheckIfRecommendationExistsAsync(int recommendationId)
        {
            return await _lifestyleRecommendationRepo.CheckIfRecommendationExistsAsync(recommendationId);
        }

        public async Task<LifestyleRecommendation> CreateLifestyleRecommendationAsync(LifestyleRecommendationRequestDTO lifestyleRecommendationRequestDTO)
        {
            var lifestyleRecommendation = _mapper.Map<LifestyleRecommendation>(lifestyleRecommendationRequestDTO);
            return await _lifestyleRecommendationRepo.CreateLifestyleRecommendationAsync(lifestyleRecommendation);
        }

        public async Task<bool> DeleteRecommendationAsync(int recommendationId)
        {
            return await _lifestyleRecommendationRepo.DeleteRecommendationAsync(recommendationId);
        }

        public async Task<LifestyleRecommendationResponseDTO> GetLifestyleRecommendationByIdAsync(int lifestyleRecommendationId)
        {
            return await _lifestyleRecommendationRepo.GetLifestyleRecommendationByIdAsync(lifestyleRecommendationId);
        }

        public async Task<List<LifestyleRecommendationResponseDTO>> GetLifestyleRecommendationsAsync()
        {
            return await _lifestyleRecommendationRepo.GetLifestyleRecommendationsAsync();
        }

        public async Task<List<LifestyleRecommendationResponseDTO>> GetRecommendationsByUsernameAsync(string username)
        {
            return await _lifestyleRecommendationRepo.GetRecommendationsByUsernameAsync(username);
        }

        public async Task<bool> UpdateRecommendationAsync(LifestyleRecommendationRequestDTO lifestyleRecommendationRequestDTO, int recommendationId)
        {
            var recommendation = _mapper.Map<LifestyleRecommendation>(lifestyleRecommendationRequestDTO);
            recommendation.LifestyleRecommendationId = recommendationId;

            return await _lifestyleRecommendationRepo.UpdateRecommendationAsync(recommendation);
        }
    }
}
