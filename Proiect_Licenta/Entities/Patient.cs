﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Proiect_Licenta.Entities
{
    public class Patient
    {
        [Key]
        public int PatientId { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        public int DoctorId { get; set; }

        // Used for EntityFramework
        public User User { get; set; }

        public Doctor Doctor { get; set; }

        public MedicalData MedicalData { get; set; }

        public ICollection<LifestyleRecommendation> LifestyleReccomendations { get; set; }

        public ICollection<MedicationPlan> MedicationPlans { get; set; }
    }
}
