﻿using System.ComponentModel.DataAnnotations;

namespace Proiect_Licenta.Entities
{
    public class MLAlgorithms
    {
        [Key]
        public int MLAlgorithmId { get; set; }

        [Required]
        public string Name { get; set; }

        public string Parameters { get; set; }
        
        [Required]
        public float Accuracy { get; set; }

        [Required]
        public float Recall { get; set; }

        [Required]
        public float Precision { get; set; }

        [Required]
        public float F1Score { get; set; }

        [Required]
        public float ROC_AUC_Score { get; set; }
    }
}
