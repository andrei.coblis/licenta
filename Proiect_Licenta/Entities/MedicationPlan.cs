﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Proiect_Licenta.Entities
{
    public class MedicationPlan
    {
        [Key]
        public int PlanId { get; set; }

        [Required]
        public int PatientId { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        [Required]
        public DateTime LastUpdated { get; set; }

        // Used for EntityFramework
        public Patient Patient { get; set; }
    }
}
