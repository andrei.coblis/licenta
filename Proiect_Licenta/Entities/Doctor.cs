﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Proiect_Licenta.Entities
{
    public class Doctor
    {
        [Key]
        public int DoctorId { get; set; }

        [Required]
        public int UserId { get; set; }

        // Used for EntityFramework
        public User User { get; set; }

        public ICollection<Patient> Patients { get; set;  }
    }
}
