﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Proiect_Licenta.Entities
{
    public class LifestyleRecommendation
    {
        [Key]
        public int LifestyleRecommendationId { get; set; }

        public string Diet { get; set; }

        public string PhysicalActivities { get; set; }

        [Required]
        public int PatientId { get; set; }

        [Required]
        public DateTime LastUpdated { get; set; }

        // Used for EntityFramework

        public Patient Patient { get; set; }
    }
}
