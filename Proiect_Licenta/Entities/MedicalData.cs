﻿using System.ComponentModel.DataAnnotations;

namespace Proiect_Licenta.Entities
{
    public class MedicalData
    {
        [Key]
        public int MedicalDataId { get; set; }

        [Required]
        public int Age { get; set; }

        [Required]
        public bool Sex { get; set; }

        [Required]
        public int ChestPain { get; set; }

        [Required]
        public int RestBp { get; set; }

        [Required]
        public int Cholesterol { get; set; }

        [Required]
        public int Fbs { get; set; }

        [Required]
        public int RestEcg { get; set; }

        [Required]
        public int Thalach { get; set; }

        [Required]
        public bool ExerciseAngina { get; set; }

        [Required]
        public float StDepression { get; set; }

        [Required]
        public int Slope { get; set; }

        [Required]
        public int NrOfBloodVessels { get; set; }

        [Required]
        public int Thalassemia { get; set; }

        public int Prediction { get; set; }

        [Required]
        public int PatientId { get; set; }

        // Used for EntityFramework

        public Patient Patient { get; set; }
    }
}
