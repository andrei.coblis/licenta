﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Proiect_Licenta.Entities
{
    public class Message
    {
        [Key]
        public int MessageId { get; set; }

        public string Username { get; set; }

        public string Sender { get; set; }

        public string Subject { get; set; }

        public string Description { get; set; }

        public DateTime SentDate { get; set; }

        // Used for EntityFramework

        public User User { get; set; }
    }
}
