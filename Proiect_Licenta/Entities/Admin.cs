﻿using System.ComponentModel.DataAnnotations;

namespace Proiect_Licenta.Entities
{
    public class Admin
    {
        [Key]
        public int AdminId { get; set; }

        [Required]
        public int UserId { get; set; }

        // Used for EntityFramework
        public User User { get; set; }
    }
}
