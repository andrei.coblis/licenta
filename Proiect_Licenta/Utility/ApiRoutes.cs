﻿namespace Proiect_Licenta.Utility
{
    public class ApiRoutes
    {
        public const string Root = "api";

        public const string Version = "v1";

        public const string Base = Root + "/" + Version;

        public static class Names
        {
            public const string GetUser = "GetUser";

            public const string GetPatient = "GetPatient";

            public const string GetDoctor = "GetDoctor";

            public const string GetAdmin = "GetAdmin";

            public const string GetMedication = "GetMedication";

            public const string GetMedicationPlan = "GetMedicationPlan";

            public const string GetLifestyleRecommendation = "GetLifestyleRecommendation";

            public const string GetMessage = "GetMessage";

            public const string GetMedicalData = "GetMedicalData";

            public const string GetMLAlgorithm = "GetMLAlgorithm";
        }

        public static class Identity
        {
            public const string Login = Base + "/login";
        }

        public static class Users
        {
            public const string GetAll = Base + "/users";

            public const string Get = Base + "/users/{userId}";

            public const string Create = Base + "/users";

            public const string Delete = Base + "/users/{userId}";

            public const string Update = Base + "/users/{userId}";
        }

        public static class Patients
        {
            public const string GetAll = Base + "/patients";

            public const string Get = Base + "/patients/{patientId}";

            public const string Create = Base + "/patients";

            public const string Update = Base + "/patients/{patientId}";

            public const string Delete = Base + "/patients/{patientId}";
        }

        public static class Doctors
        {
            public const string GetAll = Base + "/doctors";

            public const string Get = Base + "/doctors/{doctorId}";

            public const string GetStatistics = Base + "/doctors/statistics";

            public const string Create = Base + "/doctors";
        }

        public static class Admins
        {
            public const string GetAll = Base + "/admins";

            public const string Get = Base + "/admins/{adminId}";

            public const string Create = Base + "/admins";
        }

        public static class Medications
        {
            public const string GetAll = Base + "/medications";

            public const string Get = Base + "/medications/{medicationId}";

            public const string Create = Base + "/medications";
        }

        public static class MedicationPlans
        {
            public const string GetAll = Base + "/medicationPlans";

            public const string Get = Base + "/medicationPlans/{medicationPlanId}";

            public const string GetByUsername = Base + "/UserMedicationPlans/{username}";

            public const string Create = Base + "/medicationPlans";

            public const string Delete = Base + "/medicationPlans/{medicationPlanId}";

            public const string Update = Base + "/medicationPlans/{medicationPlanId}";
        }

        public static class LifestyleRecommendations
        {
            public const string GetAll = Base + "/lifestyleRecommendations";

            public const string Get = Base + "/lifestyleRecommendations/{lifestyleRecommendationId}";

            public const string GetByUsername = Base + "/UserLifestyleRecommendations/{username}";

            public const string Create = Base + "/lifestyleRecommendations";

            public const string Delete = Base + "/lifestyleRecommendations/{recommendationId}";

            public const string Update = Base + "/lifestyleRecommendations/{recommendationId}";
        }

        public static class Messages
        {
            public const string GetAll = Base + "/messages";

            public const string Get = Base + "/messages/{messageId}";

            public const string GetByUsername = Base + "/UserMessages/{username}";

            public const string Create = Base + "/messages";

            public const string Delete = Base + "/messages/{messageId}";
        }

        public static class MedicalData
        {
            public const string GetAll = Base + "/medicalData";

            public const string Get = Base + "/medicalData/{medicalDataId}";

            public const string GetByUsername = Base + "/UserMedicalData/{username}";

            public const string GetStatistics = Base + "/medicalData/statistics";

            public const string Create = Base + "/medicalData";

            public const string Delete = Base + "/medicalData/{medicalDataId}";

            public const string Update = Base + "/medicalData/{medicalDataId}";
        }

        public static class MLAlgorithm
        {
            public const string GetAll = Base + "/mlAlgorithm";

            public const string Get = Base + "/mlAlgorithm/{mlAlgorithmId}";

            public const string Delete = Base + "/mlAlgorithm/{mlAlgorithmId}";
        }
    }
}
