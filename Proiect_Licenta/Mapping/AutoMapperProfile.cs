﻿using AutoMapper;
using Proiect_Licenta.DTOs.Request;
using Proiect_Licenta.Entities;

namespace Proiect_Licenta.Mapping
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<UserRequestDTO, User>();

            CreateMap<PatientRequestDTO, Patient>();

            CreateMap<DoctorRequestDTO, Doctor>();

            CreateMap<AdminRequestDTO, Admin>();

            CreateMap<MedicationPlanRequestDTO, MedicationPlan>();

            CreateMap<LifestyleRecommendationRequestDTO, LifestyleRecommendation>();

            CreateMap<MessageRequestDTO, Message>();

            CreateMap<MedicalDataRequestDTO, MedicalData>();
        }
    }
}
